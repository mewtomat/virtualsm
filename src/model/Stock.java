package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
//import java.text.SimpleDateFormat;
import java.util.*;
import model.User;



public class Stock {

	public static class Order{
		public Integer orderID,volume;
		public Float price;
		public String type,cat,trader,stock;
		
		public Order(Integer a, Float b, Integer c, String d, String e, String f, String g){
			orderID = a; price = b; volume = c; type = d; trader = e; stock = f; cat = e;
		}
	}
	
	public static String launchStock(String company_id, String stock_id, Integer volume, Float price){
		Connection conn = null;
		String result = "";
		try{
			conn = Connect.getConnection();
			conn.setAutoCommit(false);
			PreparedStatement prepStmt;
			ResultSet rs;
			prepStmt = conn.prepareStatement("select * from stocks where stockID = ?");
			prepStmt.setString(1, stock_id);
			rs = prepStmt.executeQuery();
			conn.commit();
			if(rs.next()){
				result = "Already Exists";
			}
			else{
				prepStmt = conn.prepareStatement("insert into stocks values(?,?)");
				prepStmt.setString(1, stock_id);
				prepStmt.setString(2, company_id);
				prepStmt.executeUpdate();
				prepStmt = conn.prepareStatement("insert into portfolio values(?,?,?)");
				prepStmt.setString(1, company_id);
				prepStmt.setString(2, stock_id);
				prepStmt.setInt(3, volume);
				prepStmt.executeUpdate();
				prepStmt = conn.prepareStatement("insert into transaction(volume,time,price,seller,buyer,stock) values(?,?,?,?,?,?)");
				prepStmt.setInt(1, volume);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date(0));
				java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
				prepStmt.setTimestamp(2, ourJavaTimestampObject);
				prepStmt.setFloat(3, price);
				prepStmt.setString(4, "admin");
				prepStmt.setString(5, "admin");
				prepStmt.setString(6, stock_id);
				prepStmt.executeUpdate();
				result = "Stock Created Successfully";
				conn.commit();
				User.PlaceSellOrder(price, volume, "limit", company_id, stock_id);
			}
		}catch(Exception e) {
			System.out.println("Error while launching Stock: "+e.getMessage());
			try{
		    	conn.rollback();
		    }
		    catch(SQLException sqle){
		    	System.out.println("Error in Rollback : " + sqle.getMessage());
		    }
		}finally{
			Connect.closeConnection(conn);
		}
		return result;
	}
	
	public static List<Object[]> getGainers(){
		Connection conn = null;
		List<Object[]> result = new ArrayList<Object[]>();   
		try{
			conn = Connect.getConnection();
			PreparedStatement prepStmt;
			ResultSet rs;
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");	
//			System.out.println(sdf.format(calendar.getTime()));
			java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
			prepStmt = conn.prepareStatement("with last_price as (select t.stock as stockID, t.price as lprice from (select stock, max(tt.time) as time from transaction as tt where tt.time < ? group by stock ) as x inner join transaction as t on t.stock = x.stock and t.time = x.time),  curr_price as (select t.stock as stockID, t.price as cprice from (select stock, max(tt.time) as time from transaction as tt group by stock ) as x inner join transaction as t on t.stock = x.stock and t.time = x.time)select name, stockID, ((cprice - lprice)/lprice)*100 as change from (last_price natural join curr_price natural join stocks) inner join trader on stocks.company = trader.id order by((cprice - lprice)/lprice) limit 5;");
			prepStmt.setTimestamp(1, ourJavaTimestampObject);
			rs = prepStmt.executeQuery();
			while(rs.next()){
				Object[] o = new Object[3];
				o[0] = rs.getString(1);
				o[1] = rs.getString(2);
				o[2] = rs.getFloat(3);
				result.add(o);
			}
		}catch(Exception e) {
			System.out.println("Error while getting Gainers: "+e.getMessage());
		}finally{
			Connect.closeConnection(conn);
		}
		return result;
	}	

	public static List<Object[]> getLosers(){
		Connection conn = null;
		List<Object[]> result = new ArrayList<Object[]>();   
		try{
			conn = Connect.getConnection();
			PreparedStatement prepStmt;
			ResultSet rs;
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");	
//			System.out.println(sdf.format(calendar.getTime()));
			java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
			prepStmt = conn.prepareStatement("with last_price as (select t.stock as stockID, t.price as lprice from (select stock, max(tt.time) as time from transaction as tt where tt.time < ? group by stock ) as x inner join transaction as t on t.stock = x.stock and t.time = x.time),  curr_price as (select t.stock as stockID, t.price as cprice from (select stock, max(tt.time) as time from transaction as tt group by stock ) as x inner join transaction as t on t.stock = x.stock and t.time = x.time)select name, stockID, ((cprice - lprice)/lprice)*100 as change from (last_price natural join curr_price natural join stocks) inner join trader on stocks.company = trader.id order by((lprice - cprice)/lprice) limit 5;");
			prepStmt.setTimestamp(1, ourJavaTimestampObject);
			rs = prepStmt.executeQuery();
			while(rs.next()){
				Object[] o = new Object[3];
				o[0] = rs.getString(1);
				o[1] = rs.getString(2);
				o[2] = rs.getFloat(3);
				result.add(o);
			}
		}catch(Exception e) {
			System.out.println("Error while getting Losers: "+e.getMessage());
		}finally{
			Connect.closeConnection(conn);
		}
		return result;
	}	
	
	public static List<String[]> getStocks(){
		List<String[]> result = new ArrayList<String[]>();
		 Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     prepStmt = conn.prepareStatement("select distinct stockid, name from stocks as S join trader as C on S.company=C.id where C.cat = 'company' ");
		     ResultSet rs = prepStmt.executeQuery();
		     while(rs.next())
		     {
		    	 String[] out = new String[2];
		    	 out[0] = rs.getString(1);
		    	 out[1] = rs.getString(2);
		    	 result.add(out);
		     }
		  } catch(Exception e) {
		    System.out.println("Error while getting Stocks: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		  return result;
	}
	
	public static float getMarketPrice(String stock_id){
		Connection conn = null;
		
		try{
			conn = Connect.getConnection();
			PreparedStatement prepStmt;
			ResultSet rs;
			prepStmt = conn.prepareStatement("select price from transaction where stock=? ORDER BY time DESC LIMIT 1");
			prepStmt.setString(1, stock_id);
			rs = prepStmt.executeQuery();
			if(rs.next()){
				return rs.getFloat(1);
			}

		}catch(Exception e) {
			System.out.println("Error while getting market price: "+e.getMessage());
		}finally{
			Connect.closeConnection(conn);
		}
		return -1;
	}

	public static void BuyMarketSellMarket(String stock_id){
		Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     conn.setAutoCommit(false);
		     PreparedStatement prepStmt;
		     prepStmt = conn.prepareStatement("select * from orders where cat=cast('buy' as order_type) and type = cast('market' as order_cat) and stock = ? order by time");
		     prepStmt.setString(1, stock_id);
		     ResultSet buys = prepStmt.executeQuery();
		     prepStmt = conn.prepareStatement("select * from orders where cat=cast('sell' as order_type) and type = cast('market' as order_cat) and stock = ? order by time");
		     prepStmt.setString(1, stock_id);
		     ResultSet sells = prepStmt.executeQuery();
		     Boolean flag = false;
		     Order sell, buy;
		     if(sells.next()){ 
		    	 sell = new Order(sells.getInt(1),sells.getFloat(3),sells.getInt(4),sells.getString(5),sells.getString(6),sells.getString(7),sells.getString(8));
		    	 while(!flag && buys.next()){
		    		 buy = new Order(buys.getInt(1),buys.getFloat(3),buys.getInt(4),buys.getString(5),buys.getString(6),buys.getString(7),buys.getString(8));
		    		 while(buy.volume > 0){
			    		 if(sell.volume > 0){
			    			 String remove;
			    			 Integer min_volume = buy.volume;  
			    			 if(sell.volume > buy.volume)remove = "buys";
			    			 else if(sell.volume == buy.volume) remove = "both";
			    			 else{
			    				 remove = "sells";
			    				 min_volume = sell.volume;
			    			 }
			    			 Float market_price = getMarketPrice(stock_id);
			    			 Float buyer_balance = User.getBalance(buy.trader);
			    			 Float seller_balance = User.getBalance(sell.trader);
			    			 Integer capacity = (int) Math.floor(buyer_balance/market_price);
			    			 if(capacity < min_volume){
			    				 remove = "none";
			    				 min_volume = capacity;
			    			 }
			    			 if(capacity != 0){
			    				 // Don't execute if balance is low.
				    			 prepStmt = conn.prepareStatement("insert into transaction(volume, time, price, seller, buyer, stock) values(?,?,?,?,?,?)");
				    		     prepStmt.setInt(1, min_volume);
				    		     Calendar calendar = Calendar.getInstance();
				    			 java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
				    			 prepStmt.setTimestamp(2, ourJavaTimestampObject);
				    			 prepStmt.setFloat(3, market_price);
				    		     prepStmt.setString(4, sell.trader);
				    		     prepStmt.setString(5, buy.trader);
				    		     prepStmt.setString(6, stock_id);
				    		     prepStmt.executeUpdate();
				    		     
				    		     //decrease volume of buy order, sell order
				    		     prepStmt = conn.prepareStatement("update orders set volume=? where orderid=?");
				    		     prepStmt.setInt(1, buy.volume - min_volume);
				    		     prepStmt.setInt(2, buy.orderID);
				    		     prepStmt.executeUpdate();
				    		     
				    		     prepStmt = conn.prepareStatement("update orders set volume=? where orderid=?");
				    		     prepStmt.setInt(1, sell.volume - min_volume);
				    		     prepStmt.setInt(2, sell.orderID);
				    		     prepStmt.executeUpdate();
				    		     
				    		     buy.volume = (buy.volume - min_volume);
				    		     sell.volume = (sell.volume - min_volume);				    		     
				    		     Integer owns = User.OwnsMany(buy.trader, stock_id);
				    		     if(owns == 0){
				    		    	 prepStmt = conn.prepareStatement("insert into portfolio values(?,?,?)");
				    		    	 prepStmt.setString(1, buy.trader);
				    		    	 prepStmt.setString(2, stock_id);
				    		    	 prepStmt.setInt(3, 0);
				    		    	 prepStmt.executeUpdate();
				    		     }
				    		     prepStmt = conn.prepareStatement("update portfolio set volume = ? where owner = ? and stock = ?");
				    		     prepStmt.setInt(1, owns+min_volume);
					    		 prepStmt.setString(2, buy.trader);
			    		    	 prepStmt.setString(3, stock_id);
			    		    	 prepStmt.executeUpdate();
			    		    	 User.setBalance(buy.trader, buyer_balance - min_volume*market_price);
			    		    	 User.setBalance(sell.trader, seller_balance + min_volume*market_price);
				    		 }
			    			 if(remove.equals("buys") || remove.equals("both")){
			    		    	 prepStmt = conn.prepareStatement("delete from orders where orderID = ?");
			    		    	 prepStmt.setInt(1, buys.getInt(1));
			    		    	 prepStmt.executeUpdate();
			    		     }
			    		     if(remove.equals("sells") || remove.equals("both")){
			    		    	 prepStmt = conn.prepareStatement("delete from orders where orderID = ?");
			    		    	 prepStmt.setInt(1, sells.getInt(1));
			    		    	 prepStmt.executeUpdate();
			    		     }
			    			 if(remove.equals("none")){
			    				 if(!buys.next()) flag = true;
			    				 else buy = new Order(buys.getInt(1),buys.getFloat(3),buys.getInt(4),buys.getString(5),buys.getString(6),buys.getString(7),buys.getString(8));
					    		 
			    			 }
			    			 conn.commit();
			    		 }
			    		 else if(!sells.next()){
			    			 flag = true;
			    			 break;
			    		 }
			    		 else sell = new Order(sells.getInt(1),sells.getFloat(3),sells.getInt(4),sells.getString(5),sells.getString(6),sells.getString(7),sells.getString(8));
			    	 }
			     }
		     }
		  } catch(Exception e) {
		    System.out.println("Error in Market-Market Orders: "+e.getMessage());
		    try{
		    	conn.rollback();
		    }
		    catch(SQLException sqle){
		    	System.out.println("Error in Rollback : " + sqle.getMessage());
		    }
		  } finally {
		     Connect.closeConnection(conn);
		  }
		  return;
	}
	
	public static void BuyMarketSellOthers(String stock_id){
		Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     conn.setAutoCommit(false);
		     PreparedStatement prepStmt;
		     prepStmt = conn.prepareStatement("select * from orders where cat=cast('buy' as order_type) and type = cast('market' as order_cat) and stock = ? order by time");
		     prepStmt.setString(1, stock_id);
		     ResultSet buys = prepStmt.executeQuery();
		     prepStmt = conn.prepareStatement("select * from orders where cat=cast('sell' as order_type) and type <> cast('market' as order_cat) and stock = ? order by price asc, time desc");
		     prepStmt.setString(1, stock_id);
		     ResultSet sells = prepStmt.executeQuery();
		     Boolean flag = false;
		     Order sell, buy;
		     if(sells.next()){ 
		    	 sell = new Order(sells.getInt(1),sells.getFloat(3),sells.getInt(4),sells.getString(5),sells.getString(6),sells.getString(7),sells.getString(8));
		    	 while(!flag && buys.next()){
		    		 buy = new Order(buys.getInt(1),buys.getFloat(3),buys.getInt(4),buys.getString(5),buys.getString(6),buys.getString(7),buys.getString(8));
		    		 while(buy.volume > 0){
			    		 if(sell.volume > 0){
			    			 String remove;
			    			 Integer min_volume = buy.volume;  
			    			 if(sell.volume > buy.volume)remove = "buys";
			    			 else if(sell.volume == buy.volume) remove = "both";
			    			 else{
			    				 remove = "sells";
			    				 min_volume = sell.volume;
			    			 }
			    			 Float market_price = getMarketPrice(stock_id);
			    			 Float buyer_balance = User.getBalance(buy.trader);
			    			 Float seller_balance = User.getBalance(sell.trader);
			    			 if(sell.type.equals("stop") && sell.price < market_price){
			    				 if(!sells.next()){
					    			 flag = true;
					    			 break;
			    				 }
			    				 sell = new Order(sells.getInt(1),sells.getFloat(3),sells.getInt(4),sells.getString(5),sells.getString(6),sells.getString(7),sells.getString(8));
			    				 continue;
			    			 }
			    			 Integer capacity;
			    			 Float t_price;
			    			 if(sell.type.equals("stop")){
			    				 t_price = market_price;
			    				 capacity = (int) Math.floor(buyer_balance/market_price);
			    			 }
			    			 else{
			    				 t_price = sell.price;
			    				 capacity = (int) Math.floor(buyer_balance/sell.price);
			    			 }
			    			 if(capacity < min_volume){
			    				 remove = "none";
			    				 min_volume = capacity;
			    			 }
			    			 if(capacity != 0){
			    				 // Don't execute if balance is low.
				    			 prepStmt = conn.prepareStatement("insert into transaction(volume, time, price, seller, buyer, stock) values(?,?,?,?,?,?)");
				    		     prepStmt.setInt(1, min_volume);
				    		     Calendar calendar = Calendar.getInstance();
				    			 java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
				    			 prepStmt.setTimestamp(2, ourJavaTimestampObject);
				    			 prepStmt.setFloat(3, t_price);
				    		     prepStmt.setString(4, sell.trader);
				    		     prepStmt.setString(5, buy.trader);
				    		     prepStmt.setString(6, stock_id);
				    		     prepStmt.executeUpdate();
				    		     
				    		     //decrease volume of buy order, sell order
				    		     prepStmt = conn.prepareStatement("update orders set volume=? where orderid=?");
				    		     prepStmt.setInt(1, buy.volume - min_volume);
				    		     prepStmt.setInt(2, buy.orderID);
				    		     prepStmt.executeUpdate();
				    		     
				    		     prepStmt = conn.prepareStatement("update orders set volume=? where orderid=?");
				    		     prepStmt.setInt(1, sell.volume - min_volume);
				    		     prepStmt.setInt(2, sell.orderID);
				    		     prepStmt.executeUpdate();
				    		     
				    		     buy.volume = (buy.volume - min_volume);
				    		     sell.volume = (sell.volume - min_volume);				    		     
				    		     Integer owns = User.OwnsMany(buy.trader, stock_id);
				    		     if(owns == 0){
				    		    	 prepStmt = conn.prepareStatement("insert into portfolio values(?,?,?)");
				    		    	 prepStmt.setString(1, buy.trader);
				    		    	 prepStmt.setString(2, stock_id);
				    		    	 prepStmt.setInt(3, 0);
				    		    	 prepStmt.executeUpdate();
				    		     }
				    		     prepStmt = conn.prepareStatement("update portfolio set volume = ? where owner = ? and stock = ?");
				    		     prepStmt.setInt(1, owns+min_volume);
					    		 prepStmt.setString(2, buy.trader);
			    		    	 prepStmt.setString(3, stock_id);
			    		    	 prepStmt.executeUpdate();
			    		    	 User.setBalance(buy.trader, buyer_balance - min_volume*t_price);
			    		    	 User.setBalance(sell.trader, seller_balance + min_volume*t_price);
				    		 }
			    			 if(remove.equals("buys") || remove.equals("both")){
			    		    	 prepStmt = conn.prepareStatement("delete from orders where orderID = ?");
			    		    	 prepStmt.setInt(1, buys.getInt(1));
			    		    	 prepStmt.executeUpdate();
			    		     }
			    		     if(remove.equals("sells") || remove.equals("both")){
			    		    	 prepStmt = conn.prepareStatement("delete from orders where orderID = ?");
			    		    	 prepStmt.setInt(1, sells.getInt(1));
			    		    	 prepStmt.executeUpdate();
			    		     }
			    			 if(remove.equals("none")){
			    				 if(!buys.next()) flag = true;
			    				 else buy = new Order(buys.getInt(1),buys.getFloat(3),buys.getInt(4),buys.getString(5),buys.getString(6),buys.getString(7),buys.getString(8)); 
			    			 }
			    			 conn.commit();
			    		 }
			    		 else if(!sells.next()){
			    			 flag = true;
			    			 break;
			    		 }
			    		 else sell = new Order(sells.getInt(1),sells.getFloat(3),sells.getInt(4),sells.getString(5),sells.getString(6),sells.getString(7),sells.getString(8));
			    	 }
			     }
		     }
		  } catch(Exception e) {
		    System.out.println("Error in Market-Others Orders: "+e.getMessage());
		    try{
		    	conn.rollback();
		    }
		    catch(SQLException sqle){
		    	System.out.println("Error in Rollback : " + sqle.getMessage());
		    }
		  } finally {
		     Connect.closeConnection(conn);
		  }
		  return;
	}

	public static void BuyOthersSellMarket(String stock_id){
		Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     conn.setAutoCommit(false);
		     PreparedStatement prepStmt;
		     prepStmt = conn.prepareStatement("select * from orders where cat=cast('buy' as order_type) and type <> cast('market' as order_cat) and stock = ? order by price desc, time desc");
		     prepStmt.setString(1, stock_id);
		     ResultSet buys = prepStmt.executeQuery();
		     prepStmt = conn.prepareStatement("select * from orders where cat=cast('sell' as order_type) and type = cast('market' as order_cat) and stock = ? order by time");
		     prepStmt.setString(1, stock_id);
		     ResultSet sells = prepStmt.executeQuery();
		     Boolean flag = false;
		     Order sell, buy;
		     if(sells.next()){
		    	 sell = new Order(sells.getInt(1),sells.getFloat(3),sells.getInt(4),sells.getString(5),sells.getString(6),sells.getString(7),sells.getString(8));
		    	 while(!flag && buys.next()){
		    		 buy = new Order(buys.getInt(1),buys.getFloat(3),buys.getInt(4),buys.getString(5),buys.getString(6),buys.getString(7),buys.getString(8));
		    		 while(buy.volume > 0){
			    		 if(sell.volume > 0){
			    			 String remove;
			    			 Integer min_volume = buy.volume;  
			    			 if(sell.volume > buy.volume)remove = "buys";
			    			 else if(sell.volume == buy.volume) remove = "both";
			    			 else{
			    				 remove = "sells";
			    				 min_volume = sell.volume;
			    			 }
			    			 Float market_price = getMarketPrice(stock_id);
			    			 Float buyer_balance = User.getBalance(buy.trader);
			    			 Float seller_balance = User.getBalance(sell.trader);
			    			 if(buy.type.equals("stop") && buy.price > market_price){
			    				 if(!buys.next()){
					    			 flag = true;
					    			 break;
			    				 }
			    				 buy = new Order(buys.getInt(1),buys.getFloat(3),buys.getInt(4),buys.getString(5),buys.getString(6),buys.getString(7),buys.getString(8)); 
			    				 continue;
			    			 }
			    			 Integer capacity;
			    			 Float t_price;
			    			 if(buy.type.equals("stop")){
			    				 t_price = market_price;
			    				 capacity = (int) Math.floor(buyer_balance/market_price);
			    			 }
			    			 else{
			    				 t_price = buy.price;
			    				 capacity = (int) Math.floor(buyer_balance/buy.price);
			    			 }
			    			 if(capacity < min_volume){
			    				 remove = "none";
			    				 min_volume = capacity;
			    			 }
			    			 if(capacity != 0){
			    				 // Don't execute if balance is low.
				    			 prepStmt = conn.prepareStatement("insert into transaction(volume, time, price, seller, buyer, stock) values(?,?,?,?,?,?)");
				    		     prepStmt.setInt(1, min_volume);
				    		     Calendar calendar = Calendar.getInstance();
				    			 java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
				    			 prepStmt.setTimestamp(2, ourJavaTimestampObject);
				    			 prepStmt.setFloat(3, t_price);
				    		     prepStmt.setString(4, sell.trader);
				    		     prepStmt.setString(5, buy.trader);
				    		     prepStmt.setString(6, stock_id);
				    		     prepStmt.executeUpdate();
				    		     
				    		     //decrease volume of buy order, sell order
				    		     prepStmt = conn.prepareStatement("update orders set volume=? where orderid=?");
				    		     prepStmt.setInt(1, buy.volume - min_volume);
				    		     prepStmt.setInt(2, buy.orderID);
				    		     prepStmt.executeUpdate();
				    		     
				    		     prepStmt = conn.prepareStatement("update orders set volume=? where orderid=?");
				    		     prepStmt.setInt(1, sell.volume - min_volume);
				    		     prepStmt.setInt(2, sell.orderID);
				    		     prepStmt.executeUpdate();
				    		     
				    		     buy.volume = (buy.volume - min_volume);
				    		     sell.volume = (sell.volume - min_volume);				    		     
				    		     Integer owns = User.OwnsMany(buy.trader, stock_id);
				    		     System.out.println(owns);
				    		     if(owns == 0){
					    		     System.out.println("Here");
				    		    	 prepStmt = conn.prepareStatement("insert into portfolio values(?,?,?)");
				    		    	 prepStmt.setString(1, buy.trader);
				    		    	 prepStmt.setString(2, stock_id);
				    		    	 prepStmt.setInt(3, 0);
				    		    	 prepStmt.executeUpdate();
				    		     }
				    		     prepStmt = conn.prepareStatement("update portfolio set volume = ? where owner = ? and stock = ?");
				    		     prepStmt.setInt(1, owns+min_volume);
					    		 prepStmt.setString(2, buy.trader);
			    		    	 prepStmt.setString(3, stock_id);
			    		    	 prepStmt.executeUpdate();
			    		    	 User.setBalance(buy.trader, buyer_balance - min_volume*t_price);
			    		    	 User.setBalance(sell.trader, seller_balance + min_volume*t_price);
				    		 }
			    			 if(remove.equals("buys") || remove.equals("both")){
			    		    	 prepStmt = conn.prepareStatement("delete from orders where orderID = ?");
			    		    	 prepStmt.setInt(1, buys.getInt(1));
			    		    	 prepStmt.executeUpdate();
			    		     }
			    		     if(remove.equals("sells") || remove.equals("both")){
			    		    	 prepStmt = conn.prepareStatement("delete from orders where orderID = ?");
			    		    	 prepStmt.setInt(1, sells.getInt(1));
			    		    	 prepStmt.executeUpdate();
			    		     }
			    			 if(remove.equals("none")){
			    				 if(!buys.next()) flag = true;
			    				 else buy = new Order(buys.getInt(1),buys.getFloat(3),buys.getInt(4),buys.getString(5),buys.getString(6),buys.getString(7),buys.getString(8)); 
			    			 }
			    			 conn.commit();
			    		 }
			    		 else if(!sells.next()){
			    			 flag = true;
			    			 break;
			    		 }
			    		 else sell = new Order(sells.getInt(1),sells.getFloat(3),sells.getInt(4),sells.getString(5),sells.getString(6),sells.getString(7),sells.getString(8));
			    	 }
			     }
		     }
		  } catch(Exception e) {
		    System.out.println("Error in Others-Market Orders: "+e.getMessage());
		    try{
		    	conn.rollback();
		    }
		    catch(SQLException sqle){
		    	System.out.println("Error in Rollback : " + sqle.getMessage());
		    }
		  } finally {
		     Connect.closeConnection(conn);
		  }
		  return;
	}
	
	public static void BuyOthersSellOthers(String stock_id){
		Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     conn.setAutoCommit(false);
		     PreparedStatement prepStmt;
		     prepStmt = conn.prepareStatement("select * from orders where cat=cast('buy' as order_type) and type <> cast('market' as order_cat) and stock = ? order by price desc, time desc");
		     prepStmt.setString(1, stock_id);
		     ResultSet buys = prepStmt.executeQuery();
		     prepStmt = conn.prepareStatement("select * from orders where cat=cast('sell' as order_type) and type <> cast('market' as order_cat) and stock = ? order by price asc, time desc");
		     prepStmt.setString(1, stock_id);
		     ResultSet sells = prepStmt.executeQuery();
		     Boolean flag = false;
		     Order sell, buy;
		     if(sells.next()){
		    	 sell = new Order(sells.getInt(1),sells.getFloat(3),sells.getInt(4),sells.getString(5),sells.getString(6),sells.getString(7),sells.getString(8));
		    	 while(!flag && buys.next()){
		    		 buy = new Order(buys.getInt(1),buys.getFloat(3),buys.getInt(4),buys.getString(5),buys.getString(6),buys.getString(7),buys.getString(8));
		    		 while(buy.volume > 0){
			    		 if(sell.volume > 0){
			    			 String remove;
			    			 Integer min_volume = buy.volume;  
			    			 if(sell.volume > buy.volume)remove = "buys";
			    			 else if(sell.volume == buy.volume) remove = "both";
			    			 else{
			    				 remove = "sells";
			    				 min_volume = sell.volume;
			    			 }
			    			 Float market_price = getMarketPrice(stock_id);
			    			 Float buyer_balance = User.getBalance(buy.trader);
			    			 Float seller_balance = User.getBalance(sell.trader);
			    			 if(buy.type.equals("stop") && buy.price > market_price){
			    				 if(!buys.next()){
					    			 flag = true;
					    			 break;
			    				 }
			    				 buy = new Order(buys.getInt(1),buys.getFloat(3),buys.getInt(4),buys.getString(5),buys.getString(6),buys.getString(7),buys.getString(8)); 
			    				 continue;
			    			 }
			    			 if(sell.type.equals("stop") && sell.price < market_price){
			    				 if(!sells.next()){
					    			 flag = true;
					    			 break;
			    				 }
			    				 sell = new Order(sells.getInt(1),sells.getFloat(3),sells.getInt(4),sells.getString(5),sells.getString(6),sells.getString(7),sells.getString(8));
			    				 continue;
			    			 }
			    			 Integer capacity ;
			    			 Float t_price;
			    			 if(buy.type.equals("stop") && sell.type.equals("stop")){
			    				 t_price = market_price;
			    				 capacity = (int) Math.floor(buyer_balance/market_price);
			    			 }
			    			 else if(buy.type.equals("stop") && sell.type.equals("limit")){
			    				 t_price = sell.price;
			    				 capacity = (int) Math.floor(buyer_balance/sell.price);
			    			 }
			    			 else if(buy.type.equals("limit") && sell.type.equals("stop")){
			    				 t_price = buy.price;
			    				 capacity = (int) Math.floor(buyer_balance/buy.price);
			    			 }
			    			 else{
			    				 if(buy.price < sell.price){
			    					 if(!sells.next()){
						    			 flag = true;
						    			 break;
				    				 }
				    				 sell = new Order(sells.getInt(1),sells.getFloat(3),sells.getInt(4),sells.getString(5),sells.getString(6),sells.getString(7),sells.getString(8));
				    				 continue;
			    				 }
			    				 t_price = sell.price;
			    				 capacity = (int) Math.floor(buyer_balance/sell.price);
			    			 }
			    			 if(capacity != 0){
			    				 // Don't execute if balance is low.
				    			 prepStmt = conn.prepareStatement("insert into transaction(volume, time, price, seller, buyer, stock) values(?,?,?,?,?,?)");
				    		     prepStmt.setInt(1, min_volume);
				    		     Calendar calendar = Calendar.getInstance();
				    			 java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
				    			 prepStmt.setTimestamp(2, ourJavaTimestampObject);
				    			 prepStmt.setFloat(3, t_price);
				    		     prepStmt.setString(4, sell.trader);
				    		     prepStmt.setString(5, buy.trader);
				    		     prepStmt.setString(6, stock_id);
				    		     prepStmt.executeUpdate();
				    		     
				    		     //decrease volume of buy order, sell order
				    		     prepStmt = conn.prepareStatement("update orders set volume=? where orderid=?");
				    		     prepStmt.setInt(1, buy.volume - min_volume);
				    		     prepStmt.setInt(2, buy.orderID);
				    		     prepStmt.executeUpdate();
				    		     
				    		     prepStmt = conn.prepareStatement("update orders set volume=? where orderid=?");
				    		     prepStmt.setInt(1, sell.volume - min_volume);
				    		     prepStmt.setInt(2, sell.orderID);
				    		     prepStmt.executeUpdate();
				    		     
				    		     buy.volume = (buy.volume - min_volume);
				    		     sell.volume = (sell.volume - min_volume);				    		     
				    		     Integer owns = User.OwnsMany(buy.trader, stock_id);
				    		     if(owns == 0){
				    		    	 prepStmt = conn.prepareStatement("insert into portfolio values(?,?,?)");
				    		    	 prepStmt.setString(1, buy.trader);
				    		    	 prepStmt.setString(2, stock_id);
				    		    	 prepStmt.setInt(3, 0);
				    		    	 prepStmt.executeUpdate();
				    		     }
				    		     prepStmt = conn.prepareStatement("update portfolio set volume = ? where owner = ? and stock = ?");
				    		     prepStmt.setInt(1, owns+min_volume);
					    		 prepStmt.setString(2, buy.trader);
			    		    	 prepStmt.setString(3, stock_id);
			    		    	 prepStmt.executeUpdate();
			    		    	 User.setBalance(buy.trader, buyer_balance - min_volume*t_price);
			    		    	 User.setBalance(sell.trader, seller_balance + min_volume*t_price);
				    		 }
			    			 if(remove.equals("buys") || remove.equals("both")){
			    		    	 prepStmt = conn.prepareStatement("delete from orders where orderID = ?");
			    		    	 prepStmt.setInt(1, buys.getInt(1));
			    		    	 prepStmt.executeUpdate();
			    		     }
			    		     if(remove.equals("sells") || remove.equals("both")){
			    		    	 prepStmt = conn.prepareStatement("delete from orders where orderID = ?");
			    		    	 prepStmt.setInt(1, sells.getInt(1));
			    		    	 prepStmt.executeUpdate();
			    		     }
			    			 if(remove.equals("none")){
			    				 if(!buys.next()) flag = true;
			    				 else buy = new Order(buys.getInt(1),buys.getFloat(3),buys.getInt(4),buys.getString(5),buys.getString(6),buys.getString(7),buys.getString(8)); 
			    			 }
			    			 conn.commit();
			    		 }
			    		 else if(!sells.next()){
			    			 flag = true;
			    			 break;
			    		 }
			    		 else sell = new Order(sells.getInt(1),sells.getFloat(3),sells.getInt(4),sells.getString(5),sells.getString(6),sells.getString(7),sells.getString(8));
			    	 }
			     }
		     }
		  } catch(Exception e) {
		    System.out.println("Error in Other-Others Orders: "+e.getMessage());
		    try{
		    	conn.rollback();
		    }
		    catch(SQLException sqle){
		    	System.out.println("Error in Rollback : " + sqle.getMessage());
		    }
		  } finally {
		     Connect.closeConnection(conn);
		  }
		  return;
	}
	
	public static void ExecuteOrders(String stock_id){
		BuyMarketSellMarket(stock_id);
		BuyMarketSellOthers(stock_id);
		BuyOthersSellMarket(stock_id);
		BuyOthersSellOthers(stock_id);
		return;
	}
	
	public static List<Object[]> getPriceHistory(String stock_id){
		Connection conn = null;
		List<Object[]> result = new ArrayList<Object[]>();   
		try{
			conn = Connect.getConnection();
			PreparedStatement prepStmt;
			ResultSet rs;
			prepStmt = conn.prepareStatement("select time, price from transaction where stock = ? and not(buyer = 'admin' and seller = 'admin') order by time");
			prepStmt.setString(1, stock_id);
			rs = prepStmt.executeQuery();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss.SSS");	
			Object[] o = new Object[2];
			while(rs.next()){
				o = new Object[2];
				o[0] = (String)sdf.format(rs.getTimestamp(1));
				o[1] = rs.getFloat(2);
				result.add(o);
			}
			o[0] = (String)sdf.format(new Date());
			o[1] = getMarketPrice(stock_id);
			result.add(o);
		}catch(Exception e){
			System.out.println("Error while getting Stock History: "+e.getMessage());
		}finally{
			Connect.closeConnection(conn);
		}
		return result;
	}
	
	public static List<Object[]> processQuery (String query)
	{
		List<Object[]> result = null;
		Connection conn = null;
		System.out.println("Outside the try block");
		 try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     System.out.println("statement about to be prepared");
		     prepStmt = conn.prepareStatement("select * from stocks as S join trader as C on S.company=C.id where C.cat = 'company' and (name ~* ? or stockid ~* ? or id ~* ?) ");		//case insensitive regex
		     System.out.println("substitution about to happen");
		     prepStmt.setString(1,".*" + query+ ".*");
		     prepStmt.setString(2,".*" + query+ ".*");
		     prepStmt.setString(3,".*" + query+ ".*");
		     System.out.println("statement prepared");
		     result = new ArrayList<Object[]>() ;
		     System.out.println("statement ready to be executed");
		     ResultSet rs = prepStmt.executeQuery();
		     System.out.println("Waiting outsoide the check");
		     while(rs.next()) {
//		       System.out.println("Got some results for search !");
		       String[] rawrecord = new String[4];
		       Object[] record = null;
		       rawrecord[0] = new String(rs.getString("stockid"));
//		       rawrecord[1] = new String(Double.toString(rs.getLong("price")));
		       rawrecord[1] = new String(Float.toString(Stock.getMarketPrice(rs.getString("stockid"))));
		       rawrecord[2] = new String(rs.getString("id"));
		       rawrecord[3] = new String(rs.getString("name"));
		       
		       record =(Object[])rawrecord;
		       result.add(record);
		     }
		  } catch(Exception e) {
		    System.out.println("queryLogin: Error while processing Query: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return result;
		
	}
	
	
	public static List<Object[]> getBuyOrders(String stock_id){
		Connection conn = null;
		List<Object[]> result = new ArrayList<Object[]>();   
		try{
			conn = Connect.getConnection();
			PreparedStatement prepStmt;
			ResultSet rs;
			prepStmt = conn.prepareStatement("select volume, price, type, time from orders where stock = ? and cat = cast('buy' as order_type) order by price desc, time desc");
			prepStmt.setString(1, stock_id);
			rs = prepStmt.executeQuery();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss.SSS");	
			while(rs.next()){
				Object[] o = new Object[4];
				o[0] = rs.getInt(1);
				o[1] = rs.getFloat(2);
				o[2] = rs.getString(3);
				o[3] = (String)sdf.format(rs.getTimestamp(4));
				result.add(o);
			}
		}catch(Exception e){
			System.out.println("Error while getting Buy Orders: "+e.getMessage());
		}finally{
			Connect.closeConnection(conn);
		}
		return result;
	}
	
	public static List<Object[]> getSellOrders(String stock_id){
		Connection conn = null;
		List<Object[]> result = new ArrayList<Object[]>();   
		try{
			conn = Connect.getConnection();
			PreparedStatement prepStmt;
			ResultSet rs;
			prepStmt = conn.prepareStatement("select volume, price, type, time from orders where stock = ? and cat = cast('sell' as order_type) order by price asc, time desc");
			prepStmt.setString(1, stock_id);
			rs = prepStmt.executeQuery();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss.SSS");	
			while(rs.next()){
				Object[] o = new Object[4];
				o[0] = rs.getInt(1);
				o[1] = rs.getFloat(2);
				o[2] = rs.getString(3);
				o[3] = (String)sdf.format(rs.getTimestamp(4));
				result.add(o);
			}
		}catch(Exception e){
			System.out.println("Error while getting Sell Orders: "+e.getMessage());
		}finally{
			Connect.closeConnection(conn);
		}
		return result;
	}
	
}
