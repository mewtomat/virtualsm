// TODO: Close all resultSets and preparedStatement

package model;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class User {
	
	/* true if User with given name does not exist in database */
	public static boolean isNew(String id){
		   Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt = conn.prepareStatement("select * from trader where ID=?");
		     prepStmt.setString(1, id);
		     ResultSet rs = prepStmt.executeQuery();
		     if(rs.next()) {
		       System.out.println("User/Company " + id + " already exists in table trader");
		       return false;
		     }
//		     prepStmt = conn.prepareStatement("select * from company where ID=?");
//		     prepStmt.setString(1, id);
//		     rs = prepStmt.executeQuery();
//		     if(rs.next()) {
//		       System.out.println("User " + id + " already exists in table company");
//		       return false;
//		     }
		  } catch(Exception e) {
		    System.out.println("check user new "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		  return true;
	}
	
	
	/* returns null if authentication fails, name if authentication successful*/
	public static String authenticateLogin(String ID, String password, String category){
		 String name=null;
		 Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     prepStmt = conn.prepareStatement("select * from trader where ID=? and password=? and cat = cast(? as trader_type)"); 
		     prepStmt.setString(1, ID);
		     prepStmt.setString(2, password);
		     prepStmt.setString(3, category);
		     ResultSet rs = prepStmt.executeQuery();
		     if(rs.next()) {
		       System.out.println("User login is valid in DB");
		       name=rs.getString(3);
		     }
		  } catch(Exception e) {
		    System.out.println("validateLogin: Error while validating password: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		  return name;
	}
	/* returns null if authentication fails, name if authentication successful*/
	public static String authenticateEmail(String ID, String email, String category){
		 String name=null;
		 Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     prepStmt = conn.prepareStatement("select * from trader where ID=? and email=? and cat = cast(? as trader_type)"); 
		     prepStmt.setString(1, ID);
		     prepStmt.setString(2, email);
		     prepStmt.setString(3, category);
		     ResultSet rs = prepStmt.executeQuery();
		     if(rs.next()) {
		       System.out.println("User login is valid in DB");
		       name=rs.getString(3);
		     }
		  } catch(Exception e) {
		    System.out.println("validateLogin: Error while matching email: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		  return name;
	}
	
	/****************** given the query string(stock symbol/company name), return the summary of all stocks which match this************************/
	public static List<Object[]> processQuery (String query)
	{
		List<Object[]> result = null;
		Connection conn = null;
		System.out.println("Outside the try block");
		 try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     System.out.println("statement about to be prepared");
		     prepStmt = conn.prepareStatement("select * from stocks as S join trader as C on S.company=C.id where C.cat = 'company' and (name ~* ? or stockid ~* ? or id ~* ?) ");		//case insensitive regex
		     System.out.println("substitution about to happen");
		     prepStmt.setString(1,".*" + query+ ".*");
		     prepStmt.setString(2,".*" + query+ ".*");
		     prepStmt.setString(3,".*" + query+ ".*");
		     System.out.println("statement prepared");
		     result = new ArrayList<Object[]>() ;
		     System.out.println("statement ready to be executed");
		     ResultSet rs = prepStmt.executeQuery();
		     System.out.println("Waiting outsoide the check");
		     while(rs.next()) {
//		       System.out.println("Got some results for search !");
		       String[] rawrecord = new String[4];
		       Object[] record = null;
		       rawrecord[0] = new String(rs.getString("stockid"));
//		       rawrecord[1] = new String(Double.toString(rs.getLong("price")));
		       rawrecord[1] = new String(Float.toString(Stock.getMarketPrice(rs.getString("stockid"))));
		       rawrecord[2] = new String(rs.getString("id"));
		       rawrecord[3] = new String(rs.getString("name"));
		       
		       record =(Object[])rawrecord;
		       result.add(record);
		     }
		  } catch(Exception e) {
		    System.out.println("queryLogin: Error while processing Query: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return result;
		
	}
	
	/***** returns null if login email fail, otherwise name of the user ****/
	
	public static void register(String ID, String name, String password, String email, String address, float balance, String category){
		 Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     prepStmt = conn.prepareStatement("insert into trader values (?,?,?,?,?,?,cast(? as trader_type))");
		     prepStmt.setString(1, ID);
		     prepStmt.setString(2, address);
		     prepStmt.setString(3, name);
		     prepStmt.setString(4, password);
		     prepStmt.setString(5, email);
		     prepStmt.setFloat(6, balance);
		     prepStmt.setString(7, category);
		     prepStmt.executeUpdate();
		     
		  } catch(Exception e) {
		    System.out.println("Error while registering user: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		  return;
	}
	
	public static void updateInfo(String ID, String name, String password, String email, String address, String category){
		 Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     prepStmt = conn.prepareStatement("update trader set name=?, password=?, email =?, address=? where ID=?");
		     prepStmt.setString(1, name);
		     prepStmt.setString(2, password);
		     prepStmt.setString(3, email);
		     prepStmt.setString(4, address);
		     prepStmt.setString(5, ID);
		     prepStmt.executeUpdate();
		  } catch(Exception e) {
		    System.out.println("Error while updating Info: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		  return;
	}
	
	public static String getName(String login_id){
		Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     ResultSet rs;
		     prepStmt = conn.prepareStatement("select name from trader where ID=?");
		     prepStmt.setString(1, login_id);
		     rs = prepStmt.executeQuery();
		     if(rs.next()) {
		       return rs.getString(1);
		     }
		  } catch(Exception e) {
		    System.out.println("Error while getting Name: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return "";
	}

	public static String getPassword(String login_id){
		Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     ResultSet rs;
		     prepStmt = conn.prepareStatement("select password from trader where ID=?");
		     prepStmt.setString(1, login_id);
		     rs = prepStmt.executeQuery();
		     if(rs.next()) {
		       return rs.getString(1);
		     }
		  } catch(Exception e) {
		    System.out.println("Error while getting password: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return "";
	}
	
	public static String getAddress(String login_id){
		Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     ResultSet rs;
		     prepStmt = conn.prepareStatement("select address from trader where ID=?");
		     prepStmt.setString(1, login_id);
		     rs = prepStmt.executeQuery();
		     if(rs.next()) {
		       return rs.getString(1);
		     }
		  } catch(Exception e) {
		    System.out.println("Error while getting Address: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return "";
	}
	
	public static String getEmail(String login_id){
		Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     ResultSet rs;
		     prepStmt = conn.prepareStatement("select email from trader where ID=?");
		     prepStmt.setString(1, login_id);
		     rs = prepStmt.executeQuery();
		     if(rs.next()) {
		       return rs.getString(1);
		     }
		     
		  } catch(Exception e) {
		    System.out.println("Error while getting Email Address: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return "";
	}
	
	public static float getBalance(String login_id){
		Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     ResultSet rs;
		     prepStmt = conn.prepareStatement("select balance from trader where ID=?");
		     prepStmt.setString(1, login_id);
		     rs = prepStmt.executeQuery();
		     if(rs.next()) {
		       return rs.getFloat(1);
		     }
		     
		  } catch(Exception e) {
		    System.out.println("Error while getting Balance: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return -1;
	}
	
	public static void setBalance(String login_id, Float balance){
		Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     prepStmt = conn.prepareStatement("update trader set balance = ? where ID=?");
		     prepStmt.setFloat(1, balance);
		     prepStmt.setString(2, login_id);
		     prepStmt.executeUpdate();
		     } catch(Exception e) {
		    System.out.println("Error while setting Balance: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return;
	}
	
	public static int getShares(String login_id){
		Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     ResultSet rs;
		     prepStmt = conn.prepareStatement("select sum(volume) from portfolio where owner=?");
		     prepStmt.setString(1, login_id);
		     rs = prepStmt.executeQuery();
		     if(rs.next()) {
		       return rs.getInt(1);
		     }		     
		  } catch(Exception e) {
		    System.out.println("Error while getting Shares: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return -1;
	}

	public static int getBuyOrdersNumber(String login_id){
		Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     ResultSet rs;
		     prepStmt = conn.prepareStatement("select count(*) from orders where trader=? and cat = cast('buy' as order_type)");
		     prepStmt.setString(1, login_id);
		     rs = prepStmt.executeQuery();
		     if(rs.next()) {
		       return rs.getInt(1);
		     }		     
		  } catch(Exception e) {
		    System.out.println("Error while getting Buy Orders Number: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return -1;
	}
	public static List<Object[]> getBuyOrders(String login_id){
		Connection conn = null;
		List<Object[]> results = new ArrayList<Object[]>();
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     ResultSet rs;
		     prepStmt = conn.prepareStatement("select name, stockid, type, price, volume, time  from (select * from orders join stocks on orders.stock = stocks.stockid where trader=? and cat=cast('buy' as order_type)) as A join trader on A.company=trader.id where trader.cat=cast('company' as trader_type)");
		     prepStmt.setString(1, login_id);
		     rs = prepStmt.executeQuery();
		     while(rs.next()) {
		        Object[] o = new Object[6];
		        o[0] = rs.getString("name");
		        o[1] = rs.getString("stockid");
        		o[2] = rs.getString("type");
        		o[3] = rs.getFloat("price");
        		o[4] = rs.getInt("volume");
        		o[5] = rs.getString("time");
        		results.add(o);
		     }		     
		  } catch(Exception e) {
		    System.out.println("Error while getting BuyOrdersList Address: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return results;
	}
	
	public static List<Object[]> getSellOrders(String login_id){
		Connection conn = null;
		System.out.println("trying to get the sell orders");
		List<Object[]> results = new ArrayList<Object[]>();
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     ResultSet rs;
		     prepStmt = conn.prepareStatement("select name, stockid, type, price, volume, time  from (select * from orders join stocks on orders.stock = stocks.stockid where trader=? and cat=cast('sell' as order_type)) as A join trader on A.company=trader.id where trader.cat=cast('company' as trader_type)");
		     prepStmt.setString(1, login_id);
		     rs = prepStmt.executeQuery();
		     System.out.println("query for getting sell orders executed");
		     while(rs.next()) {
		    	 System.out.println("results found for sell ordrs!");
		        Object[] o = new Object[6];
		        o[0] = rs.getString("name");
		        o[1] = rs.getString("stockid");
        		o[2] = rs.getString("type");
        		o[3] = rs.getFloat("price");
        		o[4] = rs.getInt("volume");
        		o[5] = rs.getString("time");
        		results.add(o);
		     }		     
		  } catch(Exception e) {
		    System.out.println("Error while getting SellOrdersList Address: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return results;
	}
	
	public static int getSellOrdersNumber(String login_id){
		Connection conn = null;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     ResultSet rs;
		     prepStmt = conn.prepareStatement("select count(*) from orders where trader=? and cat = cast('sell' as order_type)");
		     prepStmt.setString(1, login_id);
		     rs = prepStmt.executeQuery();
		     if(rs.next()) {
		       return rs.getInt(1);
		     }		     
		  } catch(Exception e) {
		    System.out.println("Error while getting Sell Order Number: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return -1;
	}

	public static List<Object[]> getStockOwnership(String login_id){
		Connection conn = null;
		System.out.println("trying to get the stock ownership");
		List<Object[]> results = new ArrayList<Object[]>();
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     ResultSet rs;
		     prepStmt = conn.prepareStatement(" with stockswithcompany as (select stockid, name from stocks join trader on stocks.company=trader.id where trader.cat=cast('company' as trader_type)) select * from (select * from portfolio join stockswithcompany on portfolio.stock=stockswithcompany.stockid) as A where owner=?");
		     prepStmt.setString(1, login_id);
		     rs = prepStmt.executeQuery();
		     System.out.println("query for getting stock ownership executed");
		     while(rs.next()) {
		    	 System.out.println("results found for stock ownership!");
		        Object[] o = new Object[3];			//right now only few fields are available. later when and if changes in schema are made, this migh need to expan
		        o[0] = rs.getString("name");
		        o[1] = rs.getString("stockid");
        		o[2] = rs.getString("volume");
        		System.out.println("Results are: Name"+o[0]+" stockid: "+o[1]+" voluem: "+o[2]);
//        		o[3] = rs.getFloat("price");
//        		o[4] = rs.getInt("volume");
//        		o[5] = rs.getString("time");
        		results.add(o);
		     }		     
		  } catch(Exception e) {
		    System.out.println("Error while getting stockownership Address: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		return results;
	}
	
	public static String PlaceBuyOrder(float price, int volume, String type, String login_id, String stock_ID){
		 Connection conn = null;
		 String result;
		   try {
		     conn = Connect.getConnection();
		     conn.setAutoCommit(false);
		     PreparedStatement prepStmt;
		     Calendar calendar = Calendar.getInstance();
		     float balance = User.getBalance(login_id);
		     if(price*volume > balance){
		    	 result =  "Insufficient balance";
		     }
		     if(type.equals("market")) price = 9999; 
		     if(volume <= 0){
		    	 result = "Order Not Successful. Volume needs to be > 0";
		     }
		     else{
			     java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
			     prepStmt = conn.prepareStatement("insert into orders(time,price,volume,type,trader,stock,cat) values(?,?,?,cast(? as order_cat),?,?,cast ('buy' as order_type) )");
			     prepStmt.setTimestamp(1, ourJavaTimestampObject );
			     prepStmt.setFloat(2, price);
			     prepStmt.setInt(3, volume);
			     prepStmt.setString(4, type);
			     prepStmt.setString(5, login_id);
			     prepStmt.setString(6, stock_ID) ;
			     System.out.println(prepStmt.toString());
			     prepStmt.executeUpdate();
			     result = "Order Successfully Placed";
		     }
		     conn.commit();
		  } catch(Exception e) {
		    System.out.println("Error while Placing Order: "+e.getMessage());
		    try{
		    	conn.rollback();
		    }
		    catch(SQLException sqle){
		    	System.out.println("Error in Rollback : " + sqle.getMessage());
		    }
		    result = "Could not connect to database";
		  } finally {
		     Connect.closeConnection(conn);
		  }
		  return result;
	}
	
	public static String PlaceSellOrder(float price, int volume, String type, String login_id, String stock_ID){
		// TODO: Update stock volume. Also change the return thingy. Also remove from portfolio if placing sell order and vol goes to 0.
		 Connection conn = null;
		 String result="Order Successfully Placed";
		   try {
		     conn = Connect.getConnection();
		     conn.setAutoCommit(false);
		     PreparedStatement prepStmt;
		     Calendar calendar = Calendar.getInstance();
		     if(type.equals("market")) price = (float)0.001; 
		     prepStmt=conn.prepareStatement("select volume from portfolio where owner=? and stock=?");
		     prepStmt.setString(1, login_id);
		     prepStmt.setString(2, stock_ID);
		     ResultSet rs;
		     rs = prepStmt.executeQuery();
		     int count=0;
		     if(rs.next()) {
			       count= rs.getInt(1);
			 }
		     if(count < volume){
		    	 result= "You don't own enough stocks";
		     }
		     else if(volume <= 0){
		    	 result = "Order Not Successful. Volume needs to be > 0";
		     }
		     
		     else{
		    	 java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
		    	 prepStmt = conn.prepareStatement("insert into orders(time,price,volume,type,trader,stock,cat) values(?,?,?,cast(? as order_cat),?,?,cast ('sell' as order_type) )");
		    	 prepStmt.setTimestamp(1, ourJavaTimestampObject );
		    	 prepStmt.setFloat(2, price);
		    	 prepStmt.setInt(3, volume);
		    	 prepStmt.setString(4, type);
		    	 prepStmt.setString(5, login_id);
		    	 prepStmt.setString(6, stock_ID) ;
		    	 prepStmt.executeUpdate();
		     
		    	 if(count == volume){//remove ownership of stocks
		    		 prepStmt=conn.prepareStatement("delete from portfolio where owner=? and stock = ?");
				     prepStmt.setString(1, login_id);
				     prepStmt.setString(2, stock_ID) ;
				     prepStmt.executeUpdate();
		    	 }
		    	 else{ //reduce the volume of stocks owned
		    		 prepStmt=conn.prepareStatement("update portfolio set volume=? where owner=? and stock = ?");
		    		 prepStmt.setInt(1, count - volume);
		    		 prepStmt.setString(2, login_id);
		    		 prepStmt.setString(3, stock_ID);
		    		 prepStmt.executeUpdate();
		    	 }
		     }    
		     conn.commit();
		  } catch(Exception e) {
		    System.out.println("Error while placing order: "+e.getMessage());
		    result= "could not connect to database";
		    try{
		    	conn.rollback();
		    }
		    catch(SQLException sqle){
		    	System.out.println("Error in Rollback : " + sqle.getMessage());
		    }
		  } finally {
		     Connect.closeConnection(conn);
		  }
		  return result ;
	}

	public static Integer OwnsMany(String login_id, String stock_id){
		 Connection conn = null;
		 Integer result = 0;
		   try {
		     conn = Connect.getConnection();
		     PreparedStatement prepStmt;
		     prepStmt=conn.prepareStatement("select volume from portfolio where owner=? and stock=?");
		     prepStmt.setString(1, login_id);
		     prepStmt.setString(2, stock_id);
		     ResultSet rs;
		     rs = prepStmt.executeQuery();
		     if(rs.next()) {
			       result = rs.getInt(1);
			 }
		  } catch(Exception e) {
		    System.out.println("Error in OwnsAny: "+e.getMessage());
		  } finally {
		     Connect.closeConnection(conn);
		  }
		  return result;
	}
	
	
	public static List<Object[]> getRecentBuys(String login_id){
		Connection conn = null;
		List<Object[]> result = new ArrayList<Object[]>();   
		try{
			conn = Connect.getConnection();
			PreparedStatement prepStmt;
			ResultSet rs;
			prepStmt = conn.prepareStatement("select stock, price, volume, time from transaction where buyer = ? order by time desc limit 5");
			prepStmt.setString(1, login_id);
			rs = prepStmt.executeQuery();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");	
			while(rs.next()){
				Object[] o = new Object[4];
				o[0] = rs.getString(1);
				o[1] = rs.getFloat(2);
				o[2] = rs.getInt(3);
				o[3] = (String)sdf.format(rs.getTimestamp(4));
				result.add(o);
			}
		}catch(Exception e){
			System.out.println("Error getiing recent Buy transcations: "+e.getMessage());
		}finally{
			Connect.closeConnection(conn);
		}
		return result;
	}
	
	public static List<Object[]> getRecentSells(String login_id){
		Connection conn = null;
		List<Object[]> result = new ArrayList<Object[]>();   
		try{
			conn = Connect.getConnection();
			PreparedStatement prepStmt;
			ResultSet rs;
			prepStmt = conn.prepareStatement("select stock, price, volume, time from transaction where seller = ? order by time desc limit 5");
			prepStmt.setString(1, login_id);
			rs = prepStmt.executeQuery();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");	
			while(rs.next()){
				Object[] o = new Object[4];
				o[0] = rs.getString(1);
				o[1] = rs.getFloat(2);
				o[2] = rs.getInt(3);
				o[3] = (String)sdf.format(rs.getTimestamp(4));
				result.add(o);
			}
		}catch(Exception e){
			System.out.println("Error while getting Recent Sell transactions: "+e.getMessage());
		}finally{
			Connect.closeConnection(conn);
		}
		return result;
	}
	
	public static List<Object[]> getTransactionHistory(String login_id){
		Connection conn = null;
		List<Object[]> result = new ArrayList<Object[]>();   
		try{
			Float balance = getBalance(login_id);
			conn = Connect.getConnection();
			PreparedStatement prepStmt;
			ResultSet rs;
			prepStmt = conn.prepareStatement("select trader.name, x.stock, x.price, x.volume, x.type, x.time from ((select *, 'Buy' as type from transaction where buyer = ? union select *, 'Sell' as type from transaction where seller = ?) as x inner join stocks on x.stock = stocks.stockID) inner join trader on trader.ID = stocks.company order by time desc");
			prepStmt.setString(1, login_id);
			prepStmt.setString(2, login_id);
			rs = prepStmt.executeQuery();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss.SSS");	
			while(rs.next()){
				Object[] o = new Object[7];
				o[0] = rs.getString(1);
				o[1] = rs.getString(2);
				o[2] = rs.getFloat(3);
				o[3] = rs.getInt(4);
				o[4] = rs.getString(5);
				o[5] = balance;
				o[6] = (String)sdf.format(rs.getTimestamp(6));
				if(o[4].equals("Buy")) balance += (Float)o[2]*(Integer)o[3];
				else balance -= (Float)o[2]*(Integer)o[3];
				result.add(o);
			}
		}catch(Exception e){
			System.out.println("Error while getting Transaction History: "+e.getMessage());
		}finally{
			Connect.closeConnection(conn);
		}
		return result;
	}

	public static List<Object[]> getBalanceHistory(String login_id){
		Connection conn = null;
		List<Object[]> result = new ArrayList<Object[]>();   
		try{
			Float balance = getBalance(login_id);
			conn = Connect.getConnection();
			PreparedStatement prepStmt;
			ResultSet rs;
			prepStmt = conn.prepareStatement("select trader.name, x.stock, x.price, x.volume, x.type, x.time from ((select *, 'Buy' as type from transaction where buyer = ? union select *, 'Sell' as type from transaction where seller = ?) as x inner join stocks on x.stock = stocks.stockID) inner join trader on trader.ID = stocks.company order by time desc");
			prepStmt.setString(1, login_id);
			prepStmt.setString(2, login_id);
			rs = prepStmt.executeQuery();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss.SSS");	
			Object[] o = new Object[7];
			o[0] = "";
			o[1] = "";
			o[2] = 0.0;
			o[3] = 1;
			o[4] = "no";
			o[5] = balance;
			o[6] = (String)sdf.format(new Date());
			result.add(o);
			while(rs.next()){
				o = new Object[7];
				o[0] = rs.getString(1);
				o[1] = rs.getString(2);
				o[2] = rs.getFloat(3);
				o[3] = rs.getInt(4);
				o[4] = rs.getString(5);
				o[5] = balance;
				o[6] = (String)sdf.format(rs.getTimestamp(6));
				if(o[4].equals("Buy")) balance += (Float)o[2]*(Integer)o[3];
				else balance -= (Float)o[2]*(Integer)o[3];
				result.add(o);
			}
			Collections.reverse(result);
		}catch(Exception e){
			System.out.println("Error while getting Balance History: "+e.getMessage());
		}finally{
			Connect.closeConnection(conn);
		}
		return result;
	}

}
