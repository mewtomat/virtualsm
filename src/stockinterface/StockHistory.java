package stockinterface;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import model.Stock;
import model.User;

/**
 * Servlet implementation class StockHistory
 */
@WebServlet("/StockHistory")
public class StockHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StockHistory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Successfully called StockHistory");
		String stock_id = request.getParameter("stock_id");
		final List<Object[]> countries = Stock.getPriceHistory(stock_id);
		final List<String[]> countryList = new ArrayList<String[]>();
		
		SimpleDateFormat f = new SimpleDateFormat("yyyy MMM dd HH:mm:ss.SSS");	
		System.out.println(stock_id);
		for(int i=0;i<countries.size();i++)
		{
//			System.out.println("appending entry: " + countries.get(i)[0]);
			String[] a = new String[2];
			Date d = null;
			try {
				d = (Date) f.parse((String) countries.get(i)[0]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			a[0] = Float.toString(d.getTime());
			a[1] = Float.toString((float) countries.get(i)[1]);
			countryList.add(a);
		}
        // Map real data into JSON
        response.setContentType("application/json");
        response.getWriter().write(new Gson().toJson(countryList));
	}

}
