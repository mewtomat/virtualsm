package stockinterface;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;

/**
 * Servlet implementation class Signup
 */
@WebServlet("/Signup")
public class Signup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Signup() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		
		 request.setAttribute("valid", null);
		 request.setAttribute("display", null);
		 Boolean incomplete = false;
		 if(request.getParameter("login_id") == "")
		 {
			 System.out.println("ypu");
//				session.setAttribute("valid", "false");
//				session.setAttribute("display", "Login Id and Password should be non-empty ");
			request.setAttribute("valid", "1");
			request.setAttribute("display", "Login ID should be non-empty ");
			incomplete=true;
			request.setAttribute("code", "danger");
		 }
		 else if(request.getParameter("full_name") == "")
		 {
			 System.out.println("ypu");
//				session.setAttribute("valid", "false");
//				session.setAttribute("display", "Login Id and Password should be non-empty ");
			request.setAttribute("valid", "1");
			request.setAttribute("display", "Name should be non-empty ");
			incomplete=true;
			request.setAttribute("code", "danger");
		 }
		 else if(request.getParameter("password") == "")
		 {
			 incomplete=true;
			 request.setAttribute("valid", "1");
			 request.setAttribute("display", "Password should be non-empty");
			 request.setAttribute("code", "danger");
		 }
		 else if(request.getParameter("address") == "")
		 {
			 incomplete=true;
			 request.setAttribute("valid", "1");
			 request.setAttribute("display","Address should be non-empty");
			 request.setAttribute("code", "danger");
		 }
		 else if(request.getParameter("email") == "")
		 {
			 incomplete=true;
			 request.setAttribute("valid", "1");
			 request.setAttribute("display","Email should be non-empty");
			 request.setAttribute("code", "danger");
		 }
		  else if(request.getParameter("category") == "")
		 {
			 incomplete=true;
			 request.setAttribute("valid", "1");
			 request.setAttribute("display","Select a category");
			 request.setAttribute("code", "danger");
		 }
		 else if(!request.getParameter("password").equals(request.getParameter("confirm_password")))
		 {
			 incomplete = true;
			 request.setAttribute("valid", "1");
			 request.setAttribute("display", "Passwords do not match");
			 request.setAttribute("code", "danger");
		 }
		 if(incomplete)
		 {
			 getServletContext().getRequestDispatcher("/signup.jsp").forward(request, response);
			 return;
		 }
		//check function in model file, to check if the user is already in database
		//if user is not in database, add it to the database , give him a id, based on whether it is individual or company
		 // also remember to initialise the starting amount to some value, as suggested by rathi
		//if the operation if successful , put success to true, else to false
		//for now, put success= true
		// also, return the alloted id , place it in string Id
		String login_id=request.getParameter("login_id");
		if(!User.isNew(login_id)){
			request.setAttribute("valid","1");
			request.setAttribute("code", "danger");
			request.setAttribute("display","User/Compnay with this ID already exists");
			getServletContext().getRequestDispatcher("/signup.jsp").forward(request, response);
			return;
		}
		else
		{
		String name=request.getParameter("full_name");
		String password=request.getParameter("password");
		String email=request.getParameter("email");
		String address=request.getParameter("address");
		String category=request.getParameter("category");
		float balance = 250000;
		User.register(login_id, name, password, email, address, balance, category);
		HttpSession session = request.getSession();
		session.setAttribute("Id",login_id); // @Anmol NOTE: does this have to be different?
		session.setAttribute("name", name);
		session.setAttribute("first_user", "true");	// use this attribute to determine in the dash board, if the user if first time, then give him his id
		//		response.sendRedirect(request.getContextPath()+"/dashboard.jsp");
		getServletContext().getRequestDispatcher("/dashboard.jsp").forward(request, response);
		return;
		}
	}
}
