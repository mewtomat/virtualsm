package stockinterface;

import model.Stock;
import model.User;
import java.util.*;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Search
 */
@WebServlet("/Search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Search() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String query = request.getParameter("query");
		if(query != null)
		{
			List<Object[]> resultset =  Stock.processQuery(query);
			request.setAttribute("valid", "1");
			if (resultset!=null)
			{
				if(resultset.size() == 0)
				{
					request.setAttribute("code", "warning");
					request.setAttribute("display","No matches found");
				}
				else
				{
					request.setAttribute("code", "success");
					request.setAttribute("display", "Search Results");
				}
				request.setAttribute("searchresult", resultset);
				getServletContext().getRequestDispatcher("/market.jsp").forward(request, response);
				return;
			}
			else
			{
				request.setAttribute("valid", "1");
				request.setAttribute("code", "danger");
				request.setAttribute("display", "null pointer exception ");
				getServletContext().getRequestDispatcher("/market.jsp").forward(request, response);
				return;
			}
			
		}
		else
		{
			request.setAttribute("valid", "1");
			request.setAttribute("code", "danger");
			request.setAttribute("display", "Search string can not be empty");
			getServletContext().getRequestDispatcher("/market.jsp").forward(request, response);
			return;
			//redirect back to the market page with error : empty search title
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
