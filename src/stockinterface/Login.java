package stockinterface;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Login Request Received");
//		HttpSession session = request.getSession();
		//save message in session
		 request.setAttribute("valid", null);
		 request.setAttribute("display", null);
		 System.out.println("yo " + request.getParameter("user_id"));
		 Boolean empty = false;
		 if(request.getParameter("user_id") == "")
		{
			 empty=true;
			 System.out.println("User Id is null");
//			session.setAttribute("valid", "false");
//			session.setAttribute("display", "Login Id and Password should be non-empty ");
			request.setAttribute("valid", "1");
			request.setAttribute("code","danger");
			request.setAttribute("display", "User ID should be non-empty ");
		}
		 else if(request.getParameter("password") == "")
		 {
			 empty=true;
			 request.setAttribute("valid", "1");
			 request.setAttribute("code","danger");
			 request.setAttribute("display", "Password should be non-empty");
		 }
		 else if(request.getParameter("category") == "" || request.getParameter("category") == null)
		 {
			 empty=true;
			 request.setAttribute("valid", "1");
			 request.setAttribute("code","danger");
			 request.setAttribute("display","Select a Category");
		 }
//		System.out.println(request.getParameter("login_id"));
		if(empty){getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);return;}
		else{
			String user_id=request.getParameter("user_id");
			String password=request.getParameter("password");
			String category=request.getParameter("category");
//			String[] status = new String[1];
			String user_name = User.authenticateLogin(user_id, password, category);
//			System.out.println(login_id);
			if(user_name == null){								
				 request.setAttribute("valid", "1");
				 request.setAttribute("code","danger");
				 request.setAttribute("display","No Such User Exists");
				 System.out.println("User name returned is null");
				getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
				return;
			}
			HttpSession session=request.getSession(true);	// create the session if it does not exist
			session.setAttribute("name", user_name);
			session.setAttribute("Id",user_id );
			session.setAttribute("category", category);
			getServletContext().getRequestDispatcher("/dashboard.jsp").forward(request, response);return;
			
		}
		
//		response.sendRedirect(request.getContextPath() + "/login.jsp");
//		HttpServletResponse.s
//		doGet(request, response);
	}

}
