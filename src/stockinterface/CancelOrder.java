package stockinterface;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;

/**
 * Servlet implementation class CancelOrder
 */
@WebServlet("/CancelOrder")
public class CancelOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CancelOrder() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession(false);
		String login_id = (String)session.getAttribute("Id");
		String order_id = (String)(request.getParameter("order"));
		String volume = (String)(request.getParameter("volume"));
		String stock_id = (String)(request.getParameter("stock"));
		
		String status = User.cancelOrder(Integer.parseInt(order_id), Integer.parseInt(volume), login_id, stock_id);
		request.setAttribute("valid2", "1");
		 request.setAttribute("code2","success");
		 request.setAttribute("display","Order Successfully Cancelled");
		if(!status.equals("Order Cancelled Successfully"))
		{
			 request.setAttribute("code2","danger");
			 request.setAttribute("display","Order could not be cancelled successfully");
		}
		getServletContext().getRequestDispatcher("/portfolio.jsp").forward(request, response);return;
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
	}

}
