package stockinterface;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Stock;
import model.User;

/**
 * Servlet implementation class Trade
 */
@WebServlet("/Trade")
public class Trade extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Trade() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("Trade Request Received");
//		HttpSession session = request.getSession();
		//save message in session
		 request.setAttribute("valid", null);
		 request.setAttribute("display", null);
		 Boolean empty = false;
		 if(request.getParameter("stock_id") == "")
		{
			 empty=true;
			 System.out.println("stock Id is null");
//			session.setAttribute("valid", "false");
//			session.setAttribute("display", "Login Id and Password should be non-empty ");
			request.setAttribute("valid", "1");
			request.setAttribute("code","danger");
			request.setAttribute("display", "Select a stock symbol");
		}
		 else if(request.getParameter("transaction_type") == "")
		 {
			 empty=true;
			 request.setAttribute("valid", "1");
			 request.setAttribute("code","danger");
			 request.setAttribute("display", "Select the transaction type");
		 }
		 else if(request.getParameter("price_type") == "" || request.getParameter("price_type") == null)
		 {
			 empty=true;
			 request.setAttribute("valid", "1");
			 request.setAttribute("code","danger");
			 request.setAttribute("display","Select an order type");
		 }
		 else if(request.getParameter("quantity") == "")
		 {
			 empty=true;
			 request.setAttribute("valid", "1");
			 request.setAttribute("code","danger");
			 request.setAttribute("display","Select the quantity");
		 }
		if(empty){getServletContext().getRequestDispatcher("/trade.jsp").forward(request, response);return;}
		else{
			String stock_id=request.getParameter("stock_id");
			String price_type=request.getParameter("price_type");
			String transaction_type=request.getParameter("transaction_type");
			String quantity=request.getParameter("quantity");
			HttpSession session = request.getSession(false);
			if(session == null)
			{
				System.out.println("session can not be null, but it is");
			}
			String trader_id = (String)session.getAttribute("Id");
			Float price = null;

			if(price_type.equals("market"))
			{
				System.out.println("placing market");

				Float market_price = Stock.getMarketPrice(stock_id);
				price = new Float(market_price);
			}
			else if(price_type.equals("limit"))
			{
				price = Float.parseFloat(request.getParameter("limit_price"));
				System.out.println("placing limit");

			}
			else if(price_type.equals("stop"))
			{
				System.out.println("placing stop");

				price = Float.parseFloat(request.getParameter("stop_price"));
			}
			String status = null;	

			if(transaction_type.equals("buy"))
			{
				System.out.println("placing buy order");
				status = User.PlaceBuyOrder(price,  Integer.parseInt(quantity), price_type, trader_id, stock_id);
			}
			else
			{
				status = User.PlaceSellOrder( price, Integer.parseInt(quantity), price_type,trader_id, stock_id);
			}
			
			if(status != "Order Successfully Placed"){								
				 request.setAttribute("valid", "1");
				 request.setAttribute("code","danger");
				 request.setAttribute("display",status);
				 System.out.println("Failed to place the order");
				getServletContext().getRequestDispatcher("/trade.jsp").forward(request, response);
				return;
			}
			Stock.ExecuteOrders(stock_id);
			request.setAttribute("valid", "1");
			request.setAttribute("code", "success");
			request.setAttribute("display", status);
			getServletContext().getRequestDispatcher("/trade.jsp").forward(request, response);return;
			
		}
//		doGet(request, response);
	}

}
