package stockinterface;

import model.Stock;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * Servlet implementation class BuySellSummary
 */
@WebServlet("/BuySellSummary")
public class BuySellSummary extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuySellSummary() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("inside the buy sell summary function");
		String stock_id = (String)request.getParameter("stock_id");
		List<Object[]> buySummary = Stock.getBuyOrders(stock_id);
		List<Object[]> sellSummary = Stock.getSellOrders(stock_id);
		System.out.println(stock_id);
		System.out.println(Integer.toString(buySummary.size()));
		System.out.println(Integer.toString(sellSummary.size()));
		request.setAttribute("summary", "1");
		request.setAttribute("buySummary", buySummary);
		request.setAttribute("sellSummary", sellSummary);
		request.setAttribute("summarystock", stock_id);
		 getServletContext().getRequestDispatcher("/market.jsp").forward(request, response);
		 return ;
//		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
	}

}
