package stockinterface;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Stock;
/**
 * Servlet implementation class CreateStock
 */
@WebServlet("/CreateStock")
public class CreateStock extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateStock() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		
		HttpSession session = request.getSession(false);
		String stock_id=request.getParameter("new_stock_id");
		String stock_price=request.getParameter("new_stock_price");
		String volume=request.getParameter("initial_quantity");
		String trader_id = (String)session.getAttribute("Id");
		
		String status = Stock.launchStock(trader_id, stock_id,Integer.parseInt(volume), Float.parseFloat(stock_price));
		
		 request.setAttribute("valid2", "1");
		 request.setAttribute("code2","success");
		 request.setAttribute("display","Stocks Successfully Created");
		if(!status.equals("Stock Created Successfully"))
		{
			 request.setAttribute("code2","danger");
			 request.setAttribute("display","Stock could not be created");
		}
		getServletContext().getRequestDispatcher("/trade.jsp").forward(request, response);return;
	}

}
