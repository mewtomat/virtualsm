package stockinterface;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;

/**
 * Servlet implementation class UpdateInfo
 */
@WebServlet("/UpdateInfo")
public class UpdateInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		System.out.println("entering into update info");
		String new_name = request.getParameter("new_name");
		String new_address = request.getParameter("new_address");
		String new_email = request.getParameter("new_email");
		String new_password = request.getParameter("new_password");
		String new_confirm_password = request.getParameter("confirm_new_password");
		if(new_name=="" || new_address == "" || new_email== "" || new_password == "" || new_confirm_password == "")
		{
			request.setAttribute("valid", "1");
			request.setAttribute("code", "danger");
			request.setAttribute("display", "Entries can not be empty");
			getServletContext().getRequestDispatcher("/profile.jsp").forward(request, response);
			return;
		}
		else if(!new_confirm_password.equals(new_password))
		{
			request.setAttribute("valid", "1");
			request.setAttribute("code", "danger");
			request.setAttribute("display", "Passwords do not match");
			getServletContext().getRequestDispatcher("/profile.jsp").forward(request, response);
			return;
		}
		else
		{
			HttpSession session = request.getSession(false);
			String login_id =(String) session.getAttribute("Id");
			// Note: Doubt: are user IDs segregated according to individual/company ?
			 // currently assuming it's a user
			String category= "Individual";
			User.updateInfo(login_id, new_name, new_password, new_email, new_address, category);
			session.setAttribute("name", new_name);
			//call appropriate function from model , which takes this data and updates the database
			request.setAttribute("code", "accept");
			request.setAttribute("valid","1");
			request.setAttribute("display", "Information successfully updated");
			getServletContext().getRequestDispatcher("/profile.jsp").forward(request, response);
			return;
		}
	}

}
