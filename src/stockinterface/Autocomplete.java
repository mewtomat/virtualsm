package stockinterface;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;
import model.*;

class AutoCompleteData {
    private final String label;
    private final String value;

    public AutoCompleteData(String _label, String _value) {
        super();
        this.label = _label;
        this.value = _value;
    }

    public final String getLabel() {
        return this.label;
    }

    public final String getValue() {
        return this.value;
    }
}
/**
 * Servlet implementation class Autocomplete
 */
@WebServlet("/Autocomplete")
public class Autocomplete extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Autocomplete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Successfully called by the jquery");
		final List<String[]> countries = new ArrayList<String[]>( Stock.getStocks());
		final List<String> countryList = new ArrayList<String>();
		for(int i=0;i<countries.size();i++)
		{
//			System.out.println("appending entry: " + countries.get(i)[0]);
			countryList.add(countries.get(i)[0]);
		}
        
        Collections.sort(countryList);

        // Map real data into JSON

        response.setContentType("application/json");
        final String param = request.getParameter("term");
        final List<AutoCompleteData> result = new ArrayList<AutoCompleteData>();
        for (final String[] country : countries) {
            if (country[0].toLowerCase().startsWith(param.toLowerCase()) || country[1].toLowerCase().startsWith(param.toLowerCase())) {
                result.add(new AutoCompleteData(country[1], country[0]));
            }
        }
        response.getWriter().write(new Gson().toJson(result));
	}

}
