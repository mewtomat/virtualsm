package stockinterface;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import model.User;

/**
 * Servlet implementation class ForgotPassword
 */
@WebServlet("/ForgotPassword")
public class ForgotPassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ForgotPassword() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		System.out.println("reached here");
//		HttpSession session = request.getSession();
		//save message in session
		 request.setAttribute("valid", null);
		 request.setAttribute("display", null);
		 System.out.println("yo" + request.getParameter("login_id"));
		 Boolean empty = false;

//		System.out.println(request.getParameter("login_id"));

			String login_id=request.getParameter("login_id");
			String email_id=request.getParameter("email");
			String category=request.getParameter("category");
			    
//			String user_name = User.authenticateLogin(login_id, password, category);
			String user_name = User.authenticateEmail(login_id, email_id, category);			// Ask Rathi/Charmi to implement it
			
//			System.out.println(login_id);
			if(user_name == null){
				 request.setAttribute("valid", "1");
				 request.setAttribute("code","danger");
				 request.setAttribute("display","No Such User Exists");
				 getServletContext().getRequestDispatcher("/forgotpassword.jsp").forward(request, response);
				return;
			}
			
			/******************************************************/
						/** Send Email to the Client **/
//					      Properties props = new Properties();
//					      final String HOST = "smtp.gmail.com";
//					      final String USER = "dbapplication387@gmail.com";
//					      final String PASSWORD = "ThisisPassword";
//					      final  String PORT = "465";
//					      final String FROM = "dbapplication387@gmail.com";
//					      final String TO =  "anmolarora59@gmail.com";
//					   
//					      final String STARTTLS = "true";
//					      final String AUTH = "true";
//					      final String DEBUG = "true";
//					      final String SOCKET_FACTORY = "javax.net.ssl.SSLSocketFactory";
//					      final String SUBJECT = "Testing JavaMail API";
//					      final String TEXT = "This is a test message from my java application. Just ignore it";
//					   
//
//					      	props.put("mail.smtp.host", HOST);
//					        props.put("mail.smtp.port", PORT);
//					        props.put("mail.smtp.user", USER);
//					 
//					        props.put("mail.smtp.auth", AUTH);
//					        props.put("mail.smtp.starttls.enable", STARTTLS);
//					        props.put("mail.smtp.debug", DEBUG);
//					 
//					        props.put("mail.smtp.socketFactory.port", PORT);
//					        props.put("mail.smtp.socketFactory.class", SOCKET_FACTORY);
//					        props.put("mail.smtp.socketFactory.fallback", "false");
//					        
//						  // Set response content type
//					      response.setContentType("text/html");
////					      PrintWriter out = response.getWriter();
//			
//					      try{
//					    	  Session session = Session.getDefaultInstance(props, null);
//					            session.setDebug(true);
//					 
//					    	   MimeMessage message = new MimeMessage(session);
//					            message.setText(TEXT);
//					            message.setSubject(SUBJECT);
//					            message.setFrom(new InternetAddress(FROM));
//					            message.addRecipient(RecipientType.TO, new InternetAddress(TO));
//					            message.saveChanges();
//					 
//					            //Use Transport to deliver the message
//					            Transport transport = session.getTransport("smtp");
//					            transport.connect(HOST, USER, PASSWORD);
//					            transport.sendMessage(message, message.getAllRecipients());
//					            transport.close();
//					        
//					         
//					      }catch (MessagingException mex) {
//					          mex.printStackTrace();
//					          request.setAttribute("valid", "1");
//								 request.setAttribute("code","danger");
//								 request.setAttribute("display","Problem in sending mail");
//								 getServletContext().getRequestDispatcher("/forgotpassword.jsp").forward(request, response);
//								 return ;
//					       }
			Client client = Client.create();
		       client.addFilter(new HTTPBasicAuthFilter("api",
		                       "key-93942b6e6e7a61b6a40ac9b7f96ac096"));
		       WebResource webResource =
		               client.resource("https://api.mailgun.net/v3/sandboxe364475618ae4c14a5a7876aba6a42ce.mailgun.org" +
		                               "/messages");
		       MultivaluedMapImpl formData = new MultivaluedMapImpl();
		       formData.add("from", "CS387-DBAPP-Team <mailgun@sandboxe364475618ae4c14a5a7876aba6a42ce.mailgun.org>");
		       formData.add("to", email_id);
//		       formData.add("to", "anmolarora59@gmail.com");
		       formData.add("subject", "Your Password");
		       String password = User.getPassword(login_id);
		       formData.add("text", "Hi There! Your Password is:"+password);
		       ClientResponse clientResponse = webResource.type(MediaType.APPLICATION_FORM_URLENCODED).
		               post(ClientResponse.class, formData);
		       System.out.println(clientResponse.getStatus());
			
			/******************************************************/
			 request.setAttribute("valid", "1");
			 request.setAttribute("code","success");
			 request.setAttribute("display","Password Sent to the Email");
			 getServletContext().getRequestDispatcher("/forgotpassword.jsp").forward(request, response);
			 return ;
		
		
//		response.sendRedirect(request.getContextPath() + "/login.jsp");
//		HttpServletResponse.s
//		doGet(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
