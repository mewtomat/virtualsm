-- manually enforce that news and stocks are created only corresponding to companies.

create type trader_type as enum('individual','company');

create table trader(
 	ID varchar(10) primary key,
 	address varchar(100),
 	name varchar(100),
 	password varchar(20),
	email varchar(100),
	balance numeric(10,2),
	cat trader_type,
	check(balance >= 0)
);

--create table users(
--	primary key (ID)
--) inherits(trader);
--
--create table company(
--	primary key (ID)
--) inherits(trader);

create table stocks(
	stockID varchar(10) primary key,
	company varchar(10) references trader
);

	create table transaction(
	transactionID serial primary key,
	volume int not null,
	time timestamp not null,
	price numeric(9,5) not null,
	seller varchar(10) references trader not null, 
	buyer varchar(10) references trader not null,
	stock varchar(10) references stocks not null
);

create type order_cat as enum('market','limit','stop');
create type order_type as enum('buy','sell');

create table orders(
	orderID serial primary key, 
	time timestamp not null, 
	price numeric(9,5) not null, 
	volume int not null,
	type order_cat not null,
	trader varchar(10) references trader not null,
	stock varchar(10) references stocks not null,
	cat order_type not null
);

--create table buy_orders() inherits(orders);
--create table sell_orders() inherits(orders);

create table portfolio(
	owner varchar(10) references trader ,
	stock varchar(10) references stocks, 
--	price numeric(9,5) not null , 
	volume int not null,
	primary key (owner , stock )	
);

create table news
(
	newsID serial primary key,
	companyID varchar(10) references trader,
	time timestamp not null
); 

