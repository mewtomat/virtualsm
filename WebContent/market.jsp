<!DOCTYPE html>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Market</title>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/jqueryui/jquery-ui.min.js"></script>
<script>

	
	
	// Determine how many data points to keep based on the placeholder's initial size;
	// this gives us a nice high-res plot while avoiding more than one point per pixel.
	

	function getRandomData(stock_id) {
	    var res = [];            		
		$.ajax({
			url: "${pageContext.request.contextPath}/StockHistory",
			type: "POST",
			data: {"stock_id": stock_id}, // if session does not work, put login id here
			dataType: "json",
			success: function(data1) {
				var container = $("#flot-line-chart-moving");
				var maximum = container.outerWidth() / 2 || 300;
				var data = [];
				var ymax = 0;
				var series;
				var plot;
				
				var len = data1.length;
				console.log(stock_id);
				var i = 0;
				var min = parseFloat(data1[0][0]);
				var max = parseFloat(data1[len-1][0]);
				for (var i = 0; i < len-1; ++i) {
					ymax = (parseFloat(data1[i][1])>ymax)?parseFloat(data1[i][1]):ymax;
					res.push([(parseFloat(data1[i][0]) - min)/(max-min)*maximum, parseFloat(data1[i][1])]);
					res.push([(parseFloat(data1[i+1][0]) - min)/(max-min)*maximum, parseFloat(data1[i][1])]);
				}
				res.push([(parseFloat(data1[len-1][0]) - min)/(max-min)*maximum, parseFloat(data1[i][1])]);
	            series = [{
	                data: res,
	                lines: {
	                    fill: true
	                }
	            }];
	            
	            plot = $.plot(container, series, {
	                grid: {
	                    borderWidth: 1,
	                    minBorderMargin: 20,
	                    labelMargin: 10,
	                    backgroundColor: {
	                        colors: ["#fff", "#e4f4f4"]
	                    },
	                    margin: {
	                        top: 8,
	                        bottom: 20,
	                        left: 20
	                    },
	                    markings: function(axes) {
	                        var markings = [];
	                        var xaxis = axes.xaxis;
	                        for (var x = Math.floor(xaxis.min); x < xaxis.max; x += xaxis.tickSize * 2) {
	                            markings.push({
	                                xaxis: {
	                                    from: x,
	                                    to: x + xaxis.tickSize
	                                },
	                                color: "rgba(232, 232, 255, 0.2)"
	                            });
	                        }
	                        return markings;
	                    }
	                },
	                xaxis: {
	                    tickFormatter: function() {
	                        return "";
	                    }
	                },
	                yaxis: {
	                    min: 0,
	                    max: ymax*1.1
	                },
	                legend: {
	                    show: true
	                }
	            });
			}
		});
		
	}
</script>
<link href="bower_components/jqueryui/themes/smoothness/jquery-ui.css"
	rel="stylesheet">

<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/jqueryui/jquery-ui.min.js"></script>

<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="dist/css/sb-admin-2.css" rel="stylesheet">
<link href="bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet">

<link href="css/timeline.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="bower_components/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
	    <!-- Morris Charts CSS -->
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">
    
	<!-- DataTables CSS -->
  <link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
  <link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
	
	
<link rel="shortcut icon" href="assets/ico/favicon.png">

</head>

<body>
	<%
		String sessionId = (String)session.getAttribute("Id");
		if( sessionId == null || sessionId =="0000000000" )
		{
	%>
			<script type="text/javascript">
			window.location.href = "login.jsp";
			</script>
	<%
		}
	%>
	
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="dashboard.jsp">Virtual Stock Market</a>
			</div>
			<%
				// get all the user information here, from the servlet.
				//information includes account value, shares in holding , buy orders, sell orders, recent sell activity, 
				//recent buy activity, 5 biggest gains and 5 biggest losses
				
				
				
			%>
			<!-- /.navbar-header -->

			<ul class="nav navbar-top-links navbar-right">
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"> Hello <%= session.getAttribute("name") %> <i class="fa fa-user fa-fw"></i>
						<i class="fa fa-caret-down"></i>
				</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="profile.jsp"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
						<li class="divider"></li>
						<li><a href="${pageContext.request.contextPath}/Logout"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
					</ul> 
			</ul>

			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						<li class="sidebar-search">
							<div class="input-group custom-search-form">
								
							</div> 
						</li>
						<li><a href="dashboard.jsp"><i class="fa fa-dashboard fa-fw"></i>
								Dashboard</a></li>
						<li><a href="portfolio.jsp"><i class="fa fa-th-list fa-fw"></i>
								Portfolio</a></li>
						<li><a href="trade.jsp"><i class="fa fa-credit-card fa-fw"></i>
								Trade</a></li>
						<li><a href="market.jsp"><i class="fa fa-bar-chart-o fa-fw"></i>
								Market</a></li>
						<li><a href="profile.jsp"><i class="glyphicon  glyphicon-user glyphicon-fw"></i>
								Profile</a></li>
					</ul>
				</div>
				<!-- /.sidebar-collapse -->
			</div>
			<!-- /.navbar-static-side -->
		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">		
						<!-- if the user has just signed up, his alloted userid will be displayed in the header -->
						<!-- other wise "<user's name>'s Dashboard"  --> 
							Stock Research
						</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
      		      		
      		<div class = "container-fluid">
      		<div class="row">
      			<div class="panel">
	               <div class="panel-body">
		                <div class="col-lg-4">
		                	<form action="${pageContext.request.contextPath}/Search" method="get" role="form">
				      			<div class="input-group custom-search-form">
										<input type="text" name="query" id = "demo" class="form-control" placeholder="Enter Company Name or Stock Symbol" ></input>
											<span class="input-group-btn">
												<button class="btn btn-default right" type="submit" > <i class="fa fa-search"></i>												
												</button>
											</span>					
								</div>
								<br>
								

											<%
				                             	String is_valid = (String)request.getAttribute("valid");
				                             	String code = (String)request.getAttribute("code");
				                             	String display = (String)request.getAttribute("display");
				                             	List<Object[]> searchresults = null;
				                             	if(is_valid != null)
				                             	{
				                             %>
					                             	<div class="<%if(code=="danger") {%>alert alert-danger" <%} else if (code == "warning "){%> alert alert-warning" <%} else {%> alert alert-success" <%} %> >
														<%= request.getAttribute("display") %>
													</div>
											<%
												request.removeAttribute("display");
												request.removeAttribute("valid");
												request.removeAttribute("code");
													if(code != "danger")
													{
														searchresults = new ArrayList<Object[]>((List<Object[]>)request.getAttribute("searchresult"));
														request.removeAttribute("searchresult");	
													}
												}
											%>
										
							</form> 
									    
						</div>
					</div>
				</div>
			</div>
			
		<!-------------------------------------------------------------------------------------->
															<%
										             		String valid = (String)request.getAttribute("summary");
										             		if(valid != null)
										             		{
										             			String responseStock=(String)request.getAttribute("summarystock");
										             			if(responseStock != null)
										             			{ 
										             				List<Object[]> buy_summary = (List<Object[]>)request.getAttribute("buySummary");
										             				List<Object[]> sell_summary = (List<Object[]>)request.getAttribute("sellSummary");
										             		%>
										             		<div class="row">
										             			<div class = "page-header">
										             				<h3> Buy/Sell Orders in Market for <%= responseStock %></h3>
										             			</div>
										             		</div>
										             				 <div class="row">
														                <div class="col-lg-6">
														                    <div class="panel panel-green" id="buy_summary">
														                        <div class="panel-heading">
														                            Buy Orders
														                        </div>
														                        <div class="panel-body">
														                            <div class="dataTable_wrapper">
														                                <table class="table table-striped table-bordered table-hover" id="buysummary">
														                                    <thead>
														                                        <tr>
														                                            <th>Volume</th>
														                                            <th>Price</th>
														                                            <th>Type</th>
														                                            <th>Timestamp</th>
														                                        </tr>
														                                    </thead>
														                                    <tbody>
														                                     <%
																                                        for(Object[] row_as_array :buy_summary)
																                                        {
																                                        	out.write("<tr class=\"odd gradeX\">");
																                                        	//out.write("<td>");out.write((String)(row_as_array[0]));out.write("</td>");
																                                        	//out.write("<td><a href = ""${pageContext.request.contextPath}/Search?query="+(String)(row_as_array[1])+"\">");out.write((String)(row_as_array[1]));out.write("</a></td>");
																                                        	//
																                                        	
																                                        	//out.write("<td>");out.write((String)(row_as_array[2]));out.write("</td>");
																                                        	out.write("<td>");out.write(Integer.toString((Integer)(row_as_array[0])));out.write("</td>");
																                                        	out.write("<td>");out.write(Float.toString((Float)(row_as_array[1])));out.write("</td>");
																                                        	out.write("<td>");out.write((String)(row_as_array[2]));out.write("</td>");
																                                        	//out.write("<td>");out.write(Integer.toString((Integer)(row_as_array[4])));out.write("</td>");
																                                        	out.write("<td>");out.write((String)(row_as_array[3]));out.write("</td>");
																                                        	out.write("</tr>");
																                                        }
																                              %>
														                                    </tbody>
														                               </table>
														                            </div>     
														                        </div>
														                        <div class="panel-footer">
														                        </div>
														                    </div>
														                </div>
														                
														             	<div class="col-lg-6">
														                    <div class="panel panel-red" id="sell_summary">
														                        <div class="panel-heading">
														                            Sell Orders
														                        </div>
														                        <div class="panel-body">
														                            <div class="dataTable_wrapper">
														                                <table class="table table-striped table-bordered table-hover" id="sellsummary">
														                                    <thead>
														                                        <tr>
														                                            <th>Volume</th>
														                                            <th>Price</th>
														                                            <th>Type</th>
														                                            <th>Timestamp</th>
														                                        </tr>
														                                    </thead>
														                                    <tbody>
														                                     <%
																                                        for(Object[] row_as_array :sell_summary)
																                                        {
																                                        	out.write("<tr class=\"odd gradeX\">");
																                                        	//out.write("<td>");out.write((String)(row_as_array[0]));out.write("</td>");
																                                        	//out.write("<td><a href = ""${pageContext.request.contextPath}/Search?query="+(String)(row_as_array[1])+"\">");out.write((String)(row_as_array[1]));out.write("</a></td>");
																                                        	//
																                                        	
																                                        	//out.write("<td>");out.write((String)(row_as_array[2]));out.write("</td>");
																                                        	out.write("<td>");out.write(Integer.toString((Integer)(row_as_array[0])));out.write("</td>");
																                                        	out.write("<td>");out.write(Float.toString((Float)(row_as_array[1])));out.write("</td>");
																                                        	out.write("<td>");out.write((String)(row_as_array[2]));out.write("</td>");
																                                        	//out.write("<td>");out.write(Integer.toString((Integer)(row_as_array[4])));out.write("</td>");
																                                        	out.write("<td>");out.write((String)(row_as_array[3]));out.write("</td>");
																                                        	out.write("</tr>");
																                                        }
																                             %>
														                                    </tbody>
														                               </table>
														                            </div>     
														                        </div>
														                        <div class="panel-footer">
														                        </div>
														                    </div>
														                </div>
														                
														                
														            </div>
										             		<%	}
										             			
										             		}
										             			
										             	%>								
								
								
								
					                   <%
					             		if( searchresults != null)
					             		{
					             			int resultnumber = searchresults.size();
					             		%>
					             		<div class="row">
						             		<div class="col-lg-3">
						             			<div class="panel panel-default">
							             			<button class="btn btn-primary" type="button">
							             			 Results Found  <span class="badge"> <%= resultnumber %></span>
							             			</button>
							             		</div>
							             		
						             		</div>
					             		</div>
					             		<%	
					             			for(int i=0 ; i< searchresults.size();++i)
					             			{
					             				String[] record = (String[])searchresults.get(i);
					             				String stock_id = record[0];
					             				String price = record[1];
					             				String company_id = record[2];
					             				String company_name = record[3];
							             				
							             %>
							             <div class="row">
							             	<div class="col-lg-12">
							             		<div class= "panel panel-info">
								             		<div class="panel-heading">
							                           <button class="btn btn-warning" type="button"><%= stock_id%>	</button>							                         		
							                           <span class="pull-right"><button class="btn btn-success " type="button"><%=company_name %></button></span>
							                        </div>
							                        <div class= panel-body>
								                        <div class="row">
								                        	<div class="col-lg-3" style="text-align:center">
											             		<span style="font-weight:bold">Stock ID:</span> <%= stock_id%>
											             	</div>
											             	
								                        	<div class="col-lg-3" style="text-align:center">
											               		<span style="font-weight:bold">Company:</span> <%= company_name%>
											               		
											               	</div>
											               	
											               	<div class="col-lg-3" style="text-align:center">
											             		<span style="font-weight:bold">Last Traded Price:</span> <i class="fa fa-rupee"></i><%= price%>
											             	</div>
											             	
											             	<div class="col-lg-3" style="text-align:center;">
											             		<span style="font-weight:bold">Company ID:</span> <%=company_id %>
											             	</div>
											        
										             	</div>
										             	
										             	<div class="panel-footer">
										             		<form>
											             		<div class="col-lg-4">
											             			<a name="choice" type="button" class="btn btn-block btn-success" href="trade.jsp?stock_symbol=<%=stock_id%>">Trade</a>
											             		</div>
											             		<div class="col-lg-4" >
											             			 <button name="choice" type="button" class="btn btn-block btn-warning" onclick='getRandomData("<%=stock_id%>")'>History</button>
											             		</div>
											             		<div class="col-lg-4" >
											             			 <a href="${pageContext.request.contextPath}/BuySellSummary?stock_id=<%=stock_id%>" name="choice" type="button" class="btn btn-block btn-danger" >Buy/Sell Orders</a>
											             		</div>
											             	</form>
														</div>
									             	</div>
								             	</div>
							             	</div>
							             </div>
							             <%
							             				
							             			}
							             		}
							                        
							             %>

                       
                
              
                   
		</div>		<!-- end of container fluid -->
		
		<div class="container-fluid">
			<div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Market Watch
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="flot-chart">
                                <div class="flot-chart-content" id="flot-line-chart-moving"></div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
			
		</div>
		
		<!-- /#page-wrapper -->
		</div>
	</div>
	<!-- /#wrapper -->
	<script>
	$(document).ready(function() {
		 $(function() {
		   $("#demo").autocomplete({
	            source: function(request, response) {
	                $.ajax({
	                    url: "${pageContext.request.contextPath}/Autocomplete",
	                    type: "POST",
	                    data: { term: request.term },
	                    dataType: "json",
	                    success: function(data) {
	                        response(data);
	                    }
	               });              
	            }   
	        });
		});


            
    });
 </script>


<!-- script>
$(document).ready(function() {
    $('#sellsummary').DataTable({
            responsive: true
    });
    $('#buysummary').DataTable({
        responsive: true
	 });
});
</script-->

	<!-- Bootstrap Core JavaScript -->
	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	
  	<link rel="stylesheet" href="/resources/demos/style.css">
	<script src="dist/js/sb-admin-2.js"></script>
	<script src="bower_components/flot/excanvas.min.js"></script>
    <script src="bower_components/flot/jquery.flot.js"></script>
    <script src="bower_components/flot/jquery.flot.pie.js"></script>
    <script src="bower_components/flot/jquery.flot.resize.js"></script>
    <script src="bower_components/flot/jquery.flot.time.js"></script>
    <script src="bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>

</body>

</html>
