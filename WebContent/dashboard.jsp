<!DOCTYPE html>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.User" %>
<%@page import="model.Stock" %>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>DashBoard</title>

<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="bower_components/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="assets/ico/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<%
		String sessionId = (String)session.getAttribute("Id");
		if( sessionId == null || sessionId =="0000000000" )
		{
	%>
			<script type="text/javascript">
			window.location.href = "login.jsp";
			</script>
	<%
		}
	%>
	
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="dashboard.jsp">Virtual Stock Market</a>
			</div>
			<!-- /.navbar-header -->

			<%
				// get all the user information here, from the servlet.
				//information includes account value, shares in holding , buy orders, sell orders, recent sell activity, 
				//recent buy activity, 5 biggest gains and 5 biggest losses
				
				float account_value = User.getBalance((String)session.getAttribute("Id")); 
			    int shares_number = User.getShares((String)session.getAttribute("Id"));
				int buy_orders = User.getBuyOrdersNumber((String)session.getAttribute("Id"));
				int sell_orders = User.getSellOrdersNumber((String)session.getAttribute("Id"));
				List<Object[]> recent_buys = User.getRecentBuys((String)session.getAttribute("Id"));
				List<Object[]> recent_sells = User.getRecentSells((String)session.getAttribute("Id"));
				List<Object[]> top5_gainers = Stock.getGainers();
				List<Object[]> top5_losers = Stock.getLosers();
				
			%>

			<ul class="nav navbar-top-links navbar-right">
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"> Hello <%= session.getAttribute("name") %> <i class="fa fa-user fa-fw"></i>
						<i class="fa fa-caret-down"></i>
				</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="profile.jsp"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
						<li class="divider"></li>
						<li><a href="${pageContext.request.contextPath}/Logout"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
					</ul> 
			</ul>

			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						<li class="sidebar-search">
							<div class="input-group custom-search-form">
								
							</div> 
						</li>
						<li><a href="dashboard.jsp"><i class="fa fa-dashboard fa-fw"></i>
								Dashboard</a></li>
						<li><a href="portfolio.jsp"><i class="fa fa-th-list fa-fw"></i>
								Portfolio</a></li>
						<li><a href="trade.jsp"><i class="fa fa-credit-card fa-fw"></i>
								Trade</a></li>
						<li><a href="market.jsp"><i class="fa fa-bar-chart-o fa-fw"></i>
								Market</a></li>
						<li><a href="profile.jsp"><i class="glyphicon  glyphicon-user glyphicon-fw"></i>
								Profile</a></li>
					</ul>
				</div>
				<!-- /.sidebar-collapse -->
			</div>
			<!-- /.navbar-static-side -->
		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">		
						<!-- if the user has just signed up, his alloted userid will be displayed in the header -->
						<!-- other wise "<user's name>'s Dashboard"  --> 
						<%
							String first_time=(String)session.getAttribute("first_user");
							if(first_time != null)
							{
						%>
							Welcome New User! Your Login Id is: <%= session.getAttribute("Id") %>
						<%		
							}
							else
							{
						%>
							<%= session.getAttribute("Id") %>'s DashBoard
						<%
							}
						%>
						</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
			<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3 col-md-6">
	                    <div class="panel panel-primary">
	                        <div class="panel-heading"">
	                            <div class="row">
	                                <div class="col-xs-3">
	                                    <i class="fa fa-money fa-5x"></i>
	                                </div>
	                                <div class="col-xs-9 text-right">
	                                    <div class="huge" style="font-size:25px"><i class="fa fa-rupee"></i> <%= account_value %></div>
	                                    <div>Account Value</div>
	                                </div>
	                            </div>
	                        </div>
	                        <a href="portfolio.jsp">				<!-- redirect to the portfolio -->
	                            <div class="panel-footer">
	                                <span class="pull-left">View Details</span>
	                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                                <div class="clearfix"></div>
	                            </div>
	                        </a>
	                    </div>
	                </div>
	                
	                <div class="col-lg-3 col-md-6">
	                    <div class="panel panel-green">
	                        <div class="panel-heading">
	                            <div class="row">
	                                <div class="col-xs-3">
	                                    <i class="fa fa-suitcase fa-5x"></i>
	                                </div>
	                                <div class="col-xs-9 text-right">
	                                    <div class="huge"></i><%= shares_number %></div>
	                                    <div>Shares in holding</div>
	                                </div>
	                            </div>
	                        </div>
	                        <a href="portfolio.jsp#stock_ownership_table">				<!-- redirect to the portfolio -->
	                            <div class="panel-footer">
	                                <span class="pull-left">View Details</span>
	                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                                <div class="clearfix"></div>
	                            </div>
	                        </a>
	                    </div>
	                </div>
	                
	                 <div class="col-lg-3 col-md-6">
	                    <div class="panel panel-yellow">
	                        <div class="panel-heading">
	                            <div class="row">
	                                <div class="col-xs-3">
	                                    <i class="fa fa-shopping-cart fa-5x"></i>
	                                </div>
	                                <div class="col-xs-9 text-right">
	                                    <div class="huge"></i><%= buy_orders %></div>
	                                    <div>Buy Orders</div>
	                                </div>
	                            </div>
	                        </div>
	                        <a href="portfolio.jsp#buy_table">				<!-- redirect to the portfolio -->
	                            <div class="panel-footer">
	                                <span class="pull-left">View Details</span>
	                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                                <div class="clearfix"></div>
	                            </div>
	                        </a>
	                    </div>
	                </div>
	                
	                <div class="col-lg-3 col-md-6">
	                    <div class="panel panel-red">
	                        <div class="panel-heading">
	                            <div class="row">
	                                <div class="col-xs-3">
	                                    <i class="fa fa-external-link fa-5x"></i>
	                                </div>
	                                <div class="col-xs-9 text-right">
	                                    <div class="huge"></i><%= sell_orders %></div>
	                                    <div>Sell Orders</div>
	                                </div>
	                            </div>
	                        </div>
	                        <a href="portfolio.jsp#sell_table">				<!-- redirect to the portfolio -->
	                            <div class="panel-footer">
	                                <span class="pull-left">View Details</span>
	                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                                <div class="clearfix"></div>
	                            </div>
	                        </a>
	                    </div>
	                </div>
	              </div>
	              </div>
	              
	              <div class="container-fluid">
	              <!-- start of account summary panel -->
	                <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Recent Activity
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#buys" data-toggle="tab">Buys</a>
                                </li>
                                <li><a href="#sells" data-toggle="tab">Sells</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="buys">
                                    <div class="dataTable_wrapper">
	                                	<table class="table table-striped table-bordered table-hover" id="buys_table">
		                                    <thead>
		                                        <tr>
		                                            <th>Stock Symbol</th>
		                                            <th>Price</th>
		                                            <th>Volume</th>
		                                            <th>Date</th>
		                                            <!-- put any other field you would like to mention here -->
		                                        </tr>
		                                    </thead>
		                                    <tbody>		   
		                                        <%
		                                        for(Object[] row_as_array :recent_buys)
		                                        {
		                                        	out.write("<tr>");
		                                        	out.write("<td>");out.write((String)(row_as_array[0]));out.write("</td>");
		                                        	out.write("<td>");out.write(Float.toString((Float)(row_as_array[1])));out.write("</td>");
		                                        	out.write("<td>");out.write(Integer.toString((Integer)(row_as_array[2])));out.write("</td>");
		                                        	out.write("<td>");out.write((String)(row_as_array[3]));out.write("</td>");
		                                        	
		                                        	out.write("</tr>");
		                                        }
		                                        	//pass the information of stocks here
		                                        	//put a for loop and do
		                                        	
		                                        		//<tr class="odd gradeX">
			                                            //<td>Trident</td>
			                                            //<td>Internet Explorer 4.0</td>
			                                            //<td>Win 95+</td>
			                                            //<td class="center">4</td>
			                                            //<td class="center">X</td>
			                                        	//</tr>
			                                        // for now, printing no buys found
		                                        %>
		                                        	<!--tr>No Recent Buys</tr-->
		                                      </tbody>
	                                      </table>
	                                   </div>
	                                 </div>
                                <div class="tab-pane fade" id="sells">
                                    <div class="dataTable_wrapper">
	                                	<table class="table table-striped table-bordered table-hover" id="sells_table">
		                                    <thead>
		                                        <tr>
		                                            <th>Stock Symbol</th>
		                                            <th>Price</th>
		                                            <th>Volume</th>
		                                            <th>Date</th>
		                                            <!-- put any other field you would like to mention here -->
		                                        </tr>
		                                    </thead>
		                                    <tbody>
		                                        <%
		                                        for(Object[] row_as_array :recent_sells)
		                                        {
		                                        	out.write("<tr>");
		                                        	out.write("<td>");out.write((String)(row_as_array[0]));out.write("</td>");
		                                        	out.write("<td>");out.write(Float.toString((Float)(row_as_array[1])));out.write("</td>");
		                                        	out.write("<td>");out.write(Integer.toString((Integer)(row_as_array[2])));out.write("</td>");
		                                        	out.write("<td>");out.write((String)(row_as_array[3]));out.write("</td>");
		                                        	
		                                        	out.write("</tr>");
		                                        }
		                                        	//pass the information of stocks here
		                                        	//put a for loop and do
		                                        	
		                                        		//<tr class="odd gradeX">
			                                            //<td>Trident</td>
			                                            //<td>Internet Explorer 4.0</td>
			                                            //<td>Win 95+</td>
			                                            //<td class="center">4</td>
			                                            //<td class="center">X</td>
			                                        	//</tr>
			                                        // for now, printing no buys found
		                                        %>
		                                      </tbody>
	                                      </table>
	                                   </div>
                                </div>
                                
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- end of account summary panel -->
                
                <!-- start of market trends panel -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Stock Market Activity
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#gains" data-toggle="tab">5 Biggest Gainers</a>
                                </li>
                                <li><a href="#losses" data-toggle="tab">5 Biggest Losers</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="gains">
                                    <div class="dataTable_wrapper">
	                                	<table class="table table-striped table-bordered table-hover" id="gains_table">
		                                    <thead>
		                                        <tr>
		                                            <th>Company</th>
		                                            <th>Stock Symbol</th>
		                                            <th>Change</th>
		                                            <!-- put any other field you would like to mention here -->
		                                        </tr>
		                                    </thead>
		                                    <tbody>
		                                         <%
		                                        for(Object[] row_as_array :top5_gainers)
		                                        {
		                                        	out.write("<tr>");
		                                        %>
			                                     	<td><a href = "${pageContext.request.contextPath}/Search?query=<%= (String)(row_as_array[0])%>"><%=(String)(row_as_array[0]) %></a></td>
			                                     <%
		                                        	//out.write("<td><a href = ${page}/Search?query="+(String)(row_as_array[0])+">");out.write((String)(row_as_array[0]));out.write("</a></td>");
		                                        	out.write("<td>");out.write((String)(row_as_array[1]));out.write("</td>");
		                                        	out.write("<td>");out.write(Float.toString((Float)(row_as_array[2])));out.write("%</td>");
		                                        	out.write("</tr>");
		                                        }
		                                        %>
		                                      </tbody>
	                                      </table>
	                                   </div>
	                                 </div>
                                <div class="tab-pane fade in" id="losses">
                                    <div class="dataTable_wrapper">
	                                	<table class="table table-striped table-bordered table-hover" id="losses_table">
		                                    <thead>
		                                        <tr>
		                                            <th>Company</th>
		                                            <th>Stock Symbol</th>
		                                            <th>Change</th>
		                                            <!-- put any other field you would like to mention here -->
		                                        </tr>
		                                    </thead>
		                                    <tbody>
		                                         <%
		                                        for(Object[] row_as_array :top5_losers)
		                                        {
		                                        	out.write("<tr>");
		                                        	out.write("<td><a href = market.jsp#"+(String)(row_as_array[0])+">");out.write((String)(row_as_array[0]));out.write("</a></td>");
		                                        	out.write("<td>");out.write((String)(row_as_array[1]));out.write("</td>");
		                                        	out.write("<td>");out.write(Float.toString((Float)(row_as_array[2])));out.write("%</td>");
		                                        	out.write("</tr>");
		                                        }
		                                        %>
		                                      </tbody>
	                                      </table>
	                                   </div>
	                                 </div>
                                
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- end of market trends panel -->
                </div>
                </div>
                
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="bower_components/jquery/dist/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
