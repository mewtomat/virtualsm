<%@ page import = "model.User" %>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>DashBoard</title>

<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="bower_components/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="assets/ico/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<%
		String sessionId = (String)session.getAttribute("Id");
		if( sessionId == null || sessionId =="0000000000" )
		{
	%>
			<script type="text/javascript">
			window.location.href = "login.jsp";
			</script>
	<%
		}
	%>
	
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="dashboard.jsp">Virtual Stock Market</a>
			</div>
			<!-- /.navbar-header -->

			<ul class="nav navbar-top-links navbar-right">
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"> Hello <%= session.getAttribute("name") %> <i class="fa fa-user fa-fw"></i>
						<i class="fa fa-caret-down"></i>
				</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="profile.jsp"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
						<li class="divider"></li>
						<li><a href="${pageContext.request.contextPath}/Logout"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
					</ul> 
			</ul>

			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						<li class="sidebar-search">
							<div class="input-group custom-search-form">
								
							</div> 
						</li>
						<li><a href="dashboard.jsp"><i class="fa fa-dashboard fa-fw"></i>
								Dashboard</a></li>
						<li><a href="portfolio.jsp"><i class="fa fa-th-list fa-fw"></i>
								Portfolio</a></li>
						<li><a href="trade.jsp"><i class="fa fa-credit-card fa-fw"></i>
								Trade</a></li>
						<li><a href="market.jsp"><i class="fa fa-bar-chart-o fa-fw"></i>
								Market</a></li>
						<li><a href="profile.jsp"><i class="glyphicon  glyphicon-user glyphicon-fw"></i>
								Profile</a></li>
					</ul>
				</div>
				<!-- /.sidebar-collapse -->
			</div>
			<!-- /.navbar-static-side -->
		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">		
						<!-- if the user has just signed up, his alloted userid will be displayed in the header -->
						<!-- other wise "<user's name>'s Dashboard"  --> 
							<%= session.getAttribute("Id") %>'s Profile
						</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
      		
      		<div class="container-fluid">
      			 <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Edit Personal Information
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" action = "${pageContext.request.contextPath}/UpdateInfo" method="post">
                                        <% 
                                            	String name = User.getName((String)session.getAttribute("Id"));
                                            	String address = User.getAddress((String)session.getAttribute("Id"));
                                            	String email_id = User.getEmail((String)session.getAttribute("Id"));
                                         %>
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input class="form-control" type="text" value="<%= name%>" name="new_name"></input>
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input class="form-control" type="textarea" value="<%= address%>" name="new_address"></input>
                                        </div>
                                        <div class="form-group">
                                            <label>email_id</label>
                                            <input class="form-control" type="email" value="<%= email_id%>" name="new_email"></input>
                                        </div>
                                        <div class="form-group">
                                            <label>New Password (Enter old password if you don't want to change)</label>
                                            <input class="form-control" type="password" name="new_password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
										title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" autofocus></input>
                                        </div>
                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input class="form-control" type="password" name="confirm_new_password"></input>
                                        </div>
                                        <button type="submit" class="btn btn-default">Update Information</button>

                                     </form>
                                  </div>
                               </div>
                             </div>
                             <div class="panel-footer">
                             <%
                             	String is_valid = (String)request.getAttribute("valid");
                             	String code = (String)request.getAttribute("code");
                             	if(is_valid != null)
                             	{
                             %>
                             	<div class="<%if(code=="danger") {%>alert alert-danger" <%} else {%> alert alert-success"<%} %> >
									<%= request.getAttribute("display") %>
								</div>
							<%
								request.removeAttribute("display");
								request.removeAttribute("valid");
								request.removeAttribute("code");
								}
							%>
                             </div>
                        </div>
                    </div>
                    </div>
                                    
      		</div>
      
      
      </div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="bower_components/jquery/dist/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
