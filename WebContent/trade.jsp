<!DOCTYPE html>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.User" %>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Trading Window</title>

<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/jqueryui/jquery-ui.min.js"></script>
<link href="bower_components/jqueryui/themes/smoothness/jquery-ui.css"
	rel="stylesheet">


<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="bower_components/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="assets/ico/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->



</head>

<body>
	<%
		String sessionId = (String)session.getAttribute("Id");
		if( sessionId == null || sessionId =="0000000000" )
		{
	%>
			<script type="text/javascript">
			window.location.href = "login.jsp";
			</script>
	<%
		}
	%>
	
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="dashboard.jsp">Virtual Stock Market</a>
			</div>
			<!-- /.navbar-header -->

			<%
				// get all the user information here, from the servlet.
				//information includes account value, shares in holding , buy orders, sell orders, recent sell activity, 
				//recent buy activity, 5 biggest gains and 5 biggest losses
				
				float account_value = User.getBalance((String)session.getAttribute("Id")); 
			    int shares_number = User.getShares((String)session.getAttribute("Id"));
				int buy_orders = User.getBuyOrdersNumber((String)session.getAttribute("Id"));
				int sell_orders = User.getSellOrdersNumber((String)session.getAttribute("Id"));
				List<Object[]> recent_buys = new ArrayList<Object[]>();//call the function which returns an ArrayList(=vector) of array.each array represents a row of this table
				List<Object[]> recent_sells = new ArrayList<Object[]>();
				List<Object[]> top5_gains = new ArrayList<Object[]>();
				List<Object[]> top5_losses = new ArrayList<Object[]>();
				
			%>

			<ul class="nav navbar-top-links navbar-right">
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"> Hello <%= session.getAttribute("name") %> <i class="fa fa-user fa-fw"></i>
						<i class="fa fa-caret-down"></i>
				</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="profile.jsp"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
						<li class="divider"></li>
						<li><a href="${pageContext.request.contextPath}/Logout"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
					</ul> 
			</ul>

			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						<li class="sidebar-search">
							<div class="input-group custom-search-form">
								
							</div> 
						</li>
						<li><a href="dashboard.jsp"><i class="fa fa-dashboard fa-fw"></i>
								Dashboard</a></li>
						<li><a href="portfolio.jsp"><i class="fa fa-th-list fa-fw"></i>
								Portfolio</a></li>
						<li><a href="trade.jsp"><i class="fa fa-credit-card fa-fw"></i>
								Trade</a></li>
						<li><a href="market.jsp"><i class="fa fa-bar-chart-o fa-fw"></i>
								Market</a></li>
						<li><a href="profile.jsp"><i class="glyphicon  glyphicon-user glyphicon-fw"></i>
								Profile</a></li>
					</ul>
				</div>
				<!-- /.sidebar-collapse -->
			</div>
			<!-- /.navbar-static-side -->
		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">		
						<!-- if the user has just signed up, his alloted userid will be displayed in the header -->
						<!-- other wise "<user's name>'s Dashboard"  --> 
						Order Stock
						</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
				<div class = "row">
				<div class = "col-lg-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						<h4>Place Order</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-5">
								<form role="form" action="${pageContext.request.contextPath}/Trade" method="post">
									<div class="form-group">
	                                            <label>Stock Symbol</label>
	                                            <%
	                                            	String stock_id = (String)(request.getParameter("stock_symbol"));
	                                            %>
	                                            <input name="stock_id" id ="stock_query" class="form-control" placeholder="Stock Symbol/ Company Name" <%if(stock_id!=null){%> value="<%=stock_id%>"<%} %>>
	                                </div>
	                                 
	                                <div class="form-group">
	                                            <label>Transaction Type</label>
	                                            <select name ="transaction_type" class="form-control" >
	                                            	<option value="buy">Buy</option>
	                                            	<option value="sell">Sell</option>
	                                            </select>
	                                </div>
	                                
	                                <div class="form-group input-group">
	                                 <label>Price</label>
	                                	<div class="radio">
	                                    	<label>
	                                            <input name="price_type" type="radio" value="market" checked="checked">Market
	                                    	</label>
	                                    </div>
	                                    
	                                    <div class="radio">
	                                    	<label>
	                                            <input name="price_type" type="radio" value="limit">Limit
	                                    	</label>
	                                    </div> <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-rupee"></i></span>
	                                    	<input name="limit_price" class="form-control" placeholder="Limit Price" type="number" min="0" step="0.01">
	                                   </div>
	                                   
	                                    
	                                    <div class="radio">
	                                    	<label>
	                                            <input name="price_type" type="radio" value="stop">Stop
	                                    	</label>
	                                    </div>
	                                     <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-rupee"></i></span>
	                                    	<input name="stop_price" class="form-control" placeholder="Stop Price" type="number" min="0" step="0.01">
	                                   </div>
	                                </div>
	                                
	                                <div class="form-group">
	                                	<label>Quantity</label>
	                                	<input class="form-control" placeholder="Quantity" name="quantity" type="number" min="0">
	                                </div>
	                                
	                                <input type="submit" value="Place Order" class="btn btn-success"></input>
	                                
	                               </form>
							</div>
						</div>
						
					</div>
					<div class="panel-footer">
						<%
                             	String is_valid = (String)request.getAttribute("valid");
                             	String code = (String)request.getAttribute("code");
                             	if(is_valid != null)
                             	{
                             %>
                             	<div class="<%if(code=="danger") {%>alert alert-danger" <%} else {%> alert alert-success"<%} %> >
									<%= request.getAttribute("display") %>
								</div>
							<%
								request.removeAttribute("display");
								request.removeAttribute("valid");
								request.removeAttribute("code");
								}
							%>
						</div>
					</div>
					</div>
					</div>
				
				<% 
					String cat= (String)(session.getAttribute("category"));
					if(cat.equals("company"))
					{
				%>
					<div class = "row">
						<div class="col-lg-12">
							<div class="panel panel-red">
								<div class="panel-heading">
									<h4>Add Stocks</h4>
								</div>
								<div class="panel-body">
									<form role="form" action="${pageContext.request.contextPath}/CreateStock" method="post">
									<div class="form-group">
	                                            <label>Stock Symbol</label>
	                                            <input name="new_stock_id" class="form-control" placeholder="New Stock Symbol" type="text" maxlength="4" pattern="[A-Z]{0,}" title="All characters should be in uppercase" required="required">
	                                </div>

	                                <div class="form-group input-group">
	                                 <label>Price</label>                            
	                                     <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-rupee"></i></span>
	                                    	<input name="new_stock_price" class="form-control" placeholder="Initial Stock Price" type="number" min="0" step="0.01" required="required">
	                                   </div>
	                                </div>
	                                
	                                <div class="form-group">
	                                	<label>Quantity</label>
	                                	<input class="form-control" placeholder="Quantity" name="initial_quantity" type="number" min="0" required="required">
	                                </div>
	                                
	                                <input type="submit" value="Create Stocks" class="btn btn-success"></input>
	                                
	                               </form>
								</div>
								<div class="panel-footer">
									<%
		                             	String is_valid2 = (String)request.getAttribute("valid2");
		                             	String code2 = (String)request.getAttribute("code2");
		                             	if(is_valid2 != null)
		                             	{
		                             %>
		                             	<div class="<%if(code2=="danger") {%>alert alert-danger" <%} else {%> alert alert-success"<%} %> >
											<%= request.getAttribute("display") %>
										</div>
									<%
										request.removeAttribute("display");
										request.removeAttribute("valid2");
										request.removeAttribute("code2");
										}
									%>
								</div>
							</div>
						</div>
					</div>
				<%	
					}
				%>
				
			</div>
			<!-- /.container-fluid -->
                
                
		</div>
		<!-- /#page-wrapper -->

	</div>
	
	
	<script>
	$(document).ready(function() {
		 $(function() {
		   $("#stock_query").autocomplete({
	            source: function(request, response) {
	                $.ajax({
	                    url: "${pageContext.request.contextPath}/Autocomplete",
	                    type: "POST",
	                    data: { term: request.term },
	                    dataType: "json",
	                    success: function(data) {
	                        response(data);
	                    }
	               });              
	            }   
	        });
	});
	});
 </script>

	<!-- /#wrapper -->


	<!-- Bootstrap Core JavaScript -->
	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="dist/js/sb-admin-2.js"></script>
	
	<!-- link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css"-->
<!-- script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script-->
<!-- script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script-->

	

</body>

</html>
