<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="stockinterface.*"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Login | Virtual Stock Market</title>

<!-- Bootstrap Core CSS -->
<link href="./bower_components/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="./bower_components/metisMenu/dist/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="./dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="./bower_components/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="./assets/ico/favicon.png">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<!-- <div class = "wrapper"> -->
	<div class="navbar-header">
		<div class="navbar-brand">Virtual Stock Market</div>
	</div>
	<%
		String sessionId = (String)session.getAttribute("Id");
		if( sessionId != null && sessionId !="0000000000" )
		{
	%>
			<script type="text/javascript">
			window.location.href = "dashboard.jsp";
			</script>
	<%
		}
	%>


	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							Please Sign In <span class="glyphicon glyphicon-log-in"></span>
						</h3>
					</div>
					<div class="panel-body">
						<form role="form"
							action="${pageContext.request.contextPath}/Login" method="post">
							<fieldset>
								<div class="form-group">
									<input class="form-control" placeholder="User ID"
										name="user_id" type="text" autofocus>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Password"
										name="password" type="password" value="">
								</div>
								<div class="radio">
									<label> <input name="category" type="radio"
										value="company">Company
									</label> <label> <input name="category" type="radio"
										value="individual">Individual
									</label>
								</div>
								<!-- Change this to a button or input when using this as a form -->
								<input type="submit" class="btn btn-lg btn-success btn-block"
									value="Login" />
							</fieldset>
						</form>
					</div>
					<div class="panel-footer">
						<%
                             	String is_valid = (String)request.getAttribute("valid");
                             	String code = (String)request.getAttribute("code");
                             	if(is_valid != null)
                             	{
                             %>
                             	<div class="<%if(code=="danger") {%>alert alert-danger" <%} else {%> alert alert-success"<%} %> >
									<%= request.getAttribute("display") %>
								</div>
							<%
								request.removeAttribute("display");
								request.removeAttribute("valid");
								request.removeAttribute("code");
								}
							%>
							<div class="row">
								<div class = "col-md-6">
									<a href="signup.jsp" class="btn btn-outline btn-info">Sign Up
										Here!</a>
								</div>
								<div class = "col-md-6">
										<a href="forgotpassword.jsp" class="btn btn-outline btn-info">Forgot Password</a>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- </div> -->

	<!-- jQuery -->
	<script src="bower_components/jquery/dist/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
